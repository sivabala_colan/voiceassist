package com.newvoicesmart.Services;

import java.util.List;

import com.newvoicesmart.Utilities.Constants;
import com.newvoicesmart.Activities.TTSActivity;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver{

	String phoneNumber;
	String message;
	SharedPreferences pref;
	public void onReceive(Context context, Intent intent)
	{
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

			pref = context.getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
			String isOnOffSwitchValue = pref.getString("isOnOffSwitch", "");
			if (isOnOffSwitchValue.equalsIgnoreCase("on"))
			{
				if(Constants.haveNetworkConnection(context)){
					Editor mEditor = pref.edit();
					mEditor.putString("isIncomingStart", "Yes");
					mEditor.commit();

					Bundle bundle=intent.getExtras();
					Object[] messages=(Object[])bundle.get("pdus");
					SmsMessage[] sms=new SmsMessage[messages.length];
					for(int n=0;n<messages.length;n++){
						sms[n]=SmsMessage.createFromPdu((byte[]) messages[n]);
					}
					for(SmsMessage msg:sms){
						phoneNumber = msg.getOriginatingAddress();
						message = "\n"+ msg.getMessageBody();
					}

					ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
					List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
					//ComponentName componentInfo = taskInfo.get(0).topActivity;
					Log.d("CURRENT Activity ::" ,""+ taskInfo.get(0).topActivity.getClassName());
					String str = taskInfo.get(0).topActivity.getClassName();
					if(str.equalsIgnoreCase("com.newvoicesmart.Activities.TTSActivity")){
						TTSActivity.closeActivity();
					}
					Intent ttsActivity = new Intent(context, TTSActivity.class);
					ttsActivity.putExtra("phoneNumber", phoneNumber);
					ttsActivity.putExtra("message", message);
					ttsActivity.putExtra("isCallKey", "0");
					ttsActivity.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					ttsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(ttsActivity);
				}
			}
		}
	}
}