package com.newvoicesmart.Services;

import java.util.List;

import com.newvoicesmart.Activities.TTSActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Service;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import static android.content.Context.KEYGUARD_SERVICE;

public class IncomingCallReceiver extends BroadcastReceiver {

    AudioManager myInstance;
    SharedPreferences pref;
    KeyguardManager keyguardManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        //Toast.makeText(context, "Call receiver", Toast.LENGTH_SHORT).show();
        pref = context.getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
        String isOnOffSwitchValue = pref.getString("isOnOffSwitch", "");
        if (isOnOffSwitchValue.equalsIgnoreCase("on"))
        {
            try {
                String manufacturer = android.os.Build.MANUFACTURER;
                if(manufacturer == null)
                    manufacturer = "";
                if (!"vivo".equalsIgnoreCase(manufacturer)) {
                    keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                    TelephonyManager tm = (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
                    myInstance = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    switch (tm.getCallState()) {
                        case TelephonyManager.CALL_STATE_RINGING:

                            Editor mEditor = pref.edit();
                            mEditor.putString("isIncomingStart", "Yes");
                            mEditor.putString("isIncomingAnswered", "No");
                            mEditor.commit();

                            String phoneNumber = intent.getStringExtra("incoming_number");
                            //Toast.makeText(context, phoneNumber, Toast.LENGTH_LONG).show();

                            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                            List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                            ComponentName componentInfo = taskInfo.get(0).topActivity;
                            Log.d("CURRENT Activity ::", "" + taskInfo.get(0).topActivity.getClassName());
                            String str = taskInfo.get(0).topActivity.getClassName();
                            if (str.equalsIgnoreCase("com.newvoicesmart.Activities.TTSActivity")) {
                                TTSActivity.closeActivity();
                            }

                            myInstance.setRingerMode(AudioManager.RINGER_MODE_SILENT);

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Intent ttsActivity = new Intent(context, TTSActivity.class);
                            ttsActivity.putExtra("phoneNumber", phoneNumber);
                            ttsActivity.putExtra("message", "");
                            ttsActivity.putExtra("isCallKey", "1");
                            ttsActivity.putExtra("Close", "1");
                            ttsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            ttsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(ttsActivity);
                            break;

                        case TelephonyManager.CALL_STATE_IDLE:
                            myInstance.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                            mEditor = pref.edit();
                            mEditor.putString("isIncomingAnswered", "Yes");
                            mEditor.commit();
//                context.startActivity(new Intent(context, MainActivity.class));

//                TTSActivity.isCallStarted = false;
                            if (TTSActivity.isIncoming) {
                                Intent i = new Intent(context, TTSActivity.class);
                                i.putExtra("Close", "2"); // an extra which says to finish the activity.
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(i);
                                TTSActivity.isIncoming = false;
                            }

                            break;

                        case TelephonyManager.CALL_STATE_OFFHOOK:
                            mEditor = pref.edit();
                            mEditor.putString("isIncomingAnswered", "Yes");
                            mEditor.commit();
                    }
                }

            } catch (Exception e) {
            }
        }
    }
}