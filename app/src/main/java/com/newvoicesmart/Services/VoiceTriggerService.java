package com.newvoicesmart.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.newvoicesmart.R;
import com.newvoicesmart.Activities.TTSActivity;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import static android.widget.Toast.makeText;

public class VoiceTriggerService extends Service {

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "voice smart"; //"oh mighty computer";

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;

    public static boolean isVoiceTriggerRunning = false;
    SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {

        captions = new HashMap<>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
//        makeText(getApplicationContext(), "Preparing the recognizer", Toast.LENGTH_SHORT).show();
        // Recognizer initialization is a time-consuming and it involves IO. So we execute it in async task

        isVoiceTriggerRunning = false;
        new SetupTask(this).execute();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    private static class SetupTask extends AsyncTask<Void, Void, Exception> {
        WeakReference<VoiceTriggerService> activityReference;
        SetupTask(VoiceTriggerService activity) {
            this.activityReference = new WeakReference<>(activity);
        }
        @Override
        protected Exception doInBackground(Void... params) {
            try {
                Assets assets = new Assets(activityReference.get());
                File assetDir = assets.syncAssets();
                activityReference.get().setupRecognizer(assetDir);
            } catch (IOException e) {
                return e;
            }
            return null;
        }
        @Override
        protected void onPostExecute(Exception result) {
            if (result != null) {
                Log.e("ErrorMessage","Failed to init recognizer");
            } else {
                activityReference.get().switchSearch(KWS_SEARCH);
            }
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (recognizer != null) {
            recognizer.stop();
            recognizer.removeListener(recognitionListener);
            recognizer = null;
            isVoiceTriggerRunning = false;
        }
    }



    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        String caption = getResources().getString(captions.get(searchName));
        //makeText(getApplicationContext(), caption, Toast.LENGTH_SHORT).show();
    }

    private void setupRecognizer(File assetsDir) throws IOException {

        /*
        if (recognizer != null) {
            recognizer.stop();
            recognizer.removeListener(recognitionListener);
            recognizer = null;
        }*/
        if(recognizer == null){
            isVoiceTriggerRunning = true;
            recognizer = SpeechRecognizerSetup.defaultSetup()
                    .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                    .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                    .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                    .getRecognizer();
            recognizer.addListener(recognitionListener);

            // Create keyword-activation search.
            recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);
        }
    }



    RecognitionListener recognitionListener = new RecognitionListener() {
        /**
         * In partial result we get quick updates about current hypothesis. In
         * keyword spotting mode we can react here, in other modes we need to wait
         * for final result in onResult.
         */
        @Override
        public void onPartialResult(Hypothesis hypothesis) {
            if (hypothesis == null)
                return;

            String text = hypothesis.getHypstr();
            switch (text){
                case KEYPHRASE:
                {
                    if(recognizer != null)
                        recognizer.stop();
                    break;
                }
                default:
                    makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        }

        /**
         * This callback is called when we stop the recognizer.
         */
        @Override
        public void onResult(Hypothesis hypothesis) {
            //makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            if (hypothesis != null) {
                String text = hypothesis.getHypstr();
                if(text.equalsIgnoreCase(KEYPHRASE)){
                    stopSelf();
                    pref = getApplicationContext().getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
                    String isOnOffSwitchValue = pref.getString("isOnOffSwitch", "");
                    if (isOnOffSwitchValue.equalsIgnoreCase("on"))
                    {
                        Intent ttsActivityIntent = new Intent(getApplicationContext(), TTSActivity.class);
                        ttsActivityIntent.putExtra("KEY", "send text to contact_name");
                        ttsActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        ttsActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(ttsActivityIntent);
                    }
                    /*
                    if (recognizer != null) {
                        recognizer.cancel();
                        recognizer.shutdown();
                        recognizer.removeListener(recognitionListener);
                    }
                    isIntentServiceRunning = false;
                    */
                    //stopSelf();

                }
//                makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onBeginningOfSpeech() {
        }

        /**
         * We stop recognizer here to get a final result
         */
        @Override
        public void onEndOfSpeech() {
            if (!recognizer.getSearchName().equals(KWS_SEARCH))
                switchSearch(KWS_SEARCH);
        }

        @Override
        public void onError(Exception error) {
//            makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onTimeout() {
            switchSearch(KWS_SEARCH);
        }
    };
}
