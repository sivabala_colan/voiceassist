package com.newvoicesmart.Utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Locale;

public class Constants {

	public static String host = "smtp.gmail.com";
	public static String port = "465";
	public static String username = "dsg.colan@gmail.com";
	public static String password = "DesignColan12345";

	public static int MAppPurchareReqType = 1;
	public static String MServerUrl = "http://192.254.73.26:14056/app/active/";

	public static String languageCode(){
		String strLanguage=Locale.getDefault().getDisplayLanguage();
		String languageCode = "";
		if(strLanguage.equals("English")){
			languageCode = "en-IN";
		}else if(strLanguage.equals("Tiếng Việt")){
			languageCode = "vi-VN";
		}
		return languageCode;
	}

	public static boolean haveNetworkConnection(Context context) {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivityManager != null){
			NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
			for (NetworkInfo ni : netInfo) {
				if (ni.getTypeName().equalsIgnoreCase("WIFI"))
					if (ni.isConnected())
						haveConnectedWifi = true;
				if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
					if (ni.isConnected())
						haveConnectedMobile = true;
			}
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
}
