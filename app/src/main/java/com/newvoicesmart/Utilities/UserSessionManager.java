package com.newvoicesmart.Utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Ganeshananthan on 19-01-2017.
 */

public class UserSessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "AndroidExamplePref";
    private static final String IS_CRASH = "IsCrash";

    // Constructor
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void createCrashSession(boolean bCrash) {
        // Storing login value as TRUE
        editor.putBoolean(IS_CRASH, bCrash);

        // commit changes
        editor.commit();
    }


    /**
     * Get stored session data
     */
    public HashMap<String, Boolean> getUserDetails() {

        //Use hashmap to store user credentials
        HashMap<String, Boolean> user = new HashMap<String, Boolean>();

        //user id
        user.put(IS_CRASH, pref.getBoolean(IS_CRASH, false));

        // return user
        return user;
    }

}
