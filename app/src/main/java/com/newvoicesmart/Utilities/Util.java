package com.newvoicesmart.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.newvoicesmart.R;


public class Util {

   public static ProgressDialog MProgressDialog = null;
   public static  AlertDialog.Builder alert;
    public static void setFont(int fontNo, Activity activity, TextView view, String text) {
        String fontpath = "";
        if (fontNo == 1) {
            fontpath = "Fonts/Nirmala.ttf";
        }else if (fontNo == 2) {
            fontpath = "Fonts/NirmalaB.ttf";
        }else if (fontNo == 3) {
            fontpath = "Fonts/NirmalaS.ttf";
        }

        Typeface tf = Typeface.createFromAsset(activity.getAssets(), fontpath);
        view.setTypeface(tf);
        view.setText(text);
    }
    
    
    public static void redirectPage(Activity activity, Class<?> redirectPage){
        Intent objIntent = new Intent(activity,redirectPage);
        activity.startActivity(objIntent);
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public static void redirectFinishPage(Activity activity, Class<?> redirectPage){
        Intent objIntent = new Intent(activity,redirectPage);
        activity.startActivity(objIntent);
        activity.finish();
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public static void finishPage(Activity activity){
        activity.finish();
        activity.overridePendingTransition(R.anim.enter,R.anim.exit);
    }

    public static void showToast(Activity activity,String msg){
//        Toast.makeText(activity,msg,Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(Activity activity,String msg){
        //Toast.makeText(activity,msg,Toast.LENGTH_LONG).show();
    }
    public static void showAlertDialog(Activity activity,String message,boolean cancelable){
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle("Alert !!");
        alertDialog.setMessage(message);
        if(cancelable) {
            alertDialog.setCancelable(cancelable);
        }else{
            alertDialog.setCancelable(cancelable);
        }
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }



}
