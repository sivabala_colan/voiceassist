package com.newvoicesmart.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.newvoicesmart.R;


public class BaseActivity extends AppCompatActivity {

    protected void onCreate(Bundle saveInstance){
        super.onCreate(saveInstance);
    }

    public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return  super.onKeyDown(keyCode,keyEvent);

    }
}
