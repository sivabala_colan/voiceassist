package com.newvoicesmart.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.support.annotation.NonNull;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.instabug.library.Instabug;
import com.instabug.library.InstabugTrackingDelegate;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.newvoicesmart.Handler.ExceptionHandler;
import com.newvoicesmart.Services.IncomingCallReceiver;
import com.newvoicesmart.Services.VoiceTriggerService;
import com.newvoicesmart.Utilities.MessageDialogFragment;
import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.UserSessionManager;
import com.newvoicesmart.Utilities.Util;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements MessageDialogFragment.Listener {
    public SharedPreferences pref;
    Button voiceModeButton;
    Switch onOffSwitch;
    NotificationManager notificationManager;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 1;
    private static final int REQUEST_DO_NOT_DISTURB_PERMISSION = 2;

    private static final String FRAGMENT_MESSAGE_DIALOG = "message_dialog";
    private Context context;
    private boolean doubleBackToExitPressedOnce = false;
    UserSessionManager session;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_main);
        context = this;
        session = new UserSessionManager(this);

        new Instabug.Builder((Application) getApplicationContext(), "56622a906899436f16fa88aa92ea4050" /*94e2c5987206c54a3f564c46f49bed66*/)
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        pref = getApplicationContext().getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);

        HashMap<String, Boolean> user = session.getUserDetails();
        Boolean bCrash = user.get("IsCrash");

        if (bCrash) {
            session.createCrashSession(false);
            Intent ttsActivity = new Intent(getApplicationContext(), TTSActivity.class);
            ttsActivity.putExtra("KEY", "send text to contact_name");
            ttsActivity.putExtra("crash","yes");
            ttsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            ttsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(ttsActivity);
        } else {

        }


        // Voice Mode ON / OFF button action
        voiceModeButton = (Button) findViewById(R.id.voiceModeButton);
        voiceModeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ttsActivity = new Intent(getApplicationContext(), TTSActivity.class);
                ttsActivity.putExtra("KEY", "send text to contact_name");
                ttsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ttsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(ttsActivity);

            }
        });

        // Voice Mode ON / OFF button action
        onOffSwitch = (Switch) findViewById(R.id.on_off_switch);
        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String value = "";
                if(isChecked)
                    value = "On";
                else
                    value = "Off";
                onOffSwitch.setText(value + "  ");
                SharedPreferences.Editor mEditor = pref.edit();
                mEditor.putString("isOnOffSwitch", value);
                mEditor.commit();
                Log.v("Switch State=", ""+isChecked);
            }

        });

        if(!pref.contains("isOnOffSwitch")){
            String value = "";
            if(onOffSwitch.isChecked())
                value = "On";
            else
                value = "Off";
            onOffSwitch.setText(value + "  ");
            SharedPreferences.Editor mEditor = pref.edit();
            mEditor.putString("isOnOffSwitch", value);
            mEditor.commit();
        }
        else {
            String isOnOffSwitchValue = pref.getString("isOnOffSwitch", "");
            if (isOnOffSwitchValue.equalsIgnoreCase("on"))
                onOffSwitch.setChecked(true);
            else
                onOffSwitch.setChecked(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        int PERMISSION_ALL = 1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] PERMISSIONS = {Manifest.permission.READ_SMS, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NOTIFICATION_POLICY};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            } else {
                requestNotificationPolicyAccess();
//				startSpeechService();
            }
        } else {
//			startSpeechService();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void requestNotificationPolicyAccess() {

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!isNotificationPolicyAccessGranted()) {
                    Intent intent = new Intent(android.provider.Settings.
                            ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivityForResult(intent, REQUEST_DO_NOT_DISTURB_PERMISSION);
                }
                else {
                    String enabledNotificationListeners = Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners");
                    if (enabledNotificationListeners != null && enabledNotificationListeners.contains(getApplicationContext().getPackageName())) {
                        Log.d("Permission Granted", "ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    } else {
                        Intent i = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                        startActivity(i);
                    }
                }
            }

        } catch (Exception e) {

            Log.e("MainActivity, request", "" + e.getMessage().toString());

        }


    }

    @TargetApi(23)
    private boolean isNotificationPolicyAccessGranted() {
        notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        return notificationManager != null && notificationManager.isNotificationPolicyAccessGranted();
    }

    public void startSpeechService() {
//		if(!VoiceTriggerService.isVoiceTriggerRunning)
//			startService(new Intent(this, VoiceTriggerService.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            if (permissions.length == 9 && grantResults.length == 9
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED && grantResults[5] == PackageManager.PERMISSION_GRANTED
                    && grantResults[6] == PackageManager.PERMISSION_GRANTED && grantResults[7] == PackageManager.PERMISSION_GRANTED && grantResults[8] == PackageManager.PERMISSION_GRANTED) {
//				startSpeechService();
                requestNotificationPolicyAccess();

            } else {
                showPermissionMessageDialog();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @TargetApi(23)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DO_NOT_DISTURB_PERMISSION) {
            if (resultCode == RESULT_OK) {
                notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_PRIORITY);
                if (Build.VERSION.SDK_INT >= 24) {

                    if (Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners").contains(getApplicationContext().getPackageName())) {
                        Log.d("Permission Granted", "ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    } else {
                        Intent i = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                }
            }
        }
    }

    @Override
    public void onMessageDialogDismissed() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                REQUEST_RECORD_AUDIO_PERMISSION);
    }


    private void showPermissionMessageDialog() {
        MessageDialogFragment
                .newInstance(getString(R.string.permission_message))
                .show(getSupportFragmentManager(), FRAGMENT_MESSAGE_DIALOG);
    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Util.showToast(MainActivity.this, getString(R.string.back_again));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }


    /*public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return true;
            }

            this.doubleBackToExitPressedOnce = true;

            Util.showToast(MainActivity.this,getString(R.string.back_again));

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }
        return  super.onKeyDown(keyCode,keyEvent);

    }*/


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

}