package com.newvoicesmart.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.Util;


public class SplashScreenActivity extends BaseActivity {


    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    int SPLASH_TIME_OUT = 1000;
    private SharedPreferences sharedPreferences;
    private Activity mActivity;
    private TextView mTVSmartFVA,mTVAssistantApp,mTVWelcome;
    private LinearLayout mLLNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //INITIALIZE VIEW
        initializeView();

        //FONT TYPE
        fontType();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sharedPreferences = getSharedPreferences("VoiceSmartPref", MODE_PRIVATE);
                boolean isAppPurchase = sharedPreferences.getBoolean("isAppPurchase",false);

                Log.e(TAG,"AppPurchase :==>"+isAppPurchase);
                if(isAppPurchase){
                    Util.redirectFinishPage(mActivity,MainActivity.class);
                }else{
                    mLLNext.setVisibility(View.VISIBLE);
                }

            }
        }, SPLASH_TIME_OUT);



        //CLICK LISTENER.
        mLLNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.redirectFinishPage(mActivity,InformationActivity.class);
            }
        });

    }


    //INITIALIZE VIEW
    private void initializeView(){
        mActivity = SplashScreenActivity.this;
        mTVSmartFVA = findViewById(R.id.txt_smart_fva);
        mLLNext = findViewById(R.id.linear_next);
        mTVAssistantApp = findViewById(R.id.txt_assistant_app);
        mTVWelcome = findViewById(R.id.txt_welcome);
    }


    //SET FONT FOR VIEW
    private void fontType(){
        Util.setFont(2,mActivity,mTVSmartFVA,getString(R.string.smart_fva));
        Util.setFont(1,mActivity,mTVAssistantApp,getString(R.string.assistant_app));
        Util.setFont(1,mActivity,mTVWelcome,getString(R.string.welcome));

    }


}
