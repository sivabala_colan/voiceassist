package com.newvoicesmart.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.Constants;
import com.newvoicesmart.Utilities.Util;
import com.newvoicesmart.Utilities.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity {

    private Button mBTNSubmit;
    private EditText mETKey;
    private String keyValue = "";
    private Activity mActivity;
    private SharedPreferences mPref;
    private ProgressDialog mProgress;
    private int status = -1;
    private boolean type = false;
    public static final String TAG = LoginActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //INITIALIZE VIEW
        initializeView();

        //FONT TYPE
        fontType();

        //ONCLICK LISTENER
        mBTNSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyValue = mETKey.getText().toString();
                if(!keyValue.equals("")){
                    if(Constants.haveNetworkConnection(mActivity)) {
                        new LoginWebservice().execute();
                    }else{
                        //Toast.makeText(getApplicationContext(),getString(R.string.check_internet),Toast.LENGTH_SHORT).show();
                        Util.showAlertDialog(mActivity,getString(R.string.check_internet),true);
                    }
                }else{
                    mETKey.setError(getString(R.string.enter_key));
                }

            }
        });

    }

    //INITIALIZE VIEW
    private void initializeView(){
        mActivity = LoginActivity.this;
        mETKey = findViewById(R.id.edt_key);
        mBTNSubmit = findViewById(R.id.btn_submit);
    }

    //SET FONT FOR VIEW
    private void fontType(){
        Util.setFont(1,mActivity,mETKey,"");
        Util.setFont(2,mActivity,mBTNSubmit,getString(R.string.submit));

    }
    private class LoginWebservice extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            mProgress = new ProgressDialog(mActivity);
            mProgress.setMessage("Please wait...");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(Constants.MServerUrl+keyValue);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                     status = jsonObj.getInt("status");
                     type = jsonObj.getBoolean("type");
                     Log.e(TAG, "Json Status: " + status);
                     Log.e(TAG, "Json type: " + type);



                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getApplicationContext(),"Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getApplicationContext(),"Couldn't get json from server. Check LogCat for possible errors!",Toast.LENGTH_LONG).show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (mProgress.isShowing())
                mProgress.dismiss();

            if(status == 1){
                mPref = mActivity.getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor mEditor = mPref.edit();
                mEditor.putBoolean("isAppPurchase", type);
                mEditor.commit();
                 Util.redirectFinishPage(mActivity,MainActivity.class);
            }else{
                //Toast.makeText(getApplicationContext(),getString(R.string.incorrect_key),Toast.LENGTH_SHORT).show();
            }

        }
    }


    public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            Util.redirectFinishPage(mActivity,AppPurchaseActivity.class);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return  super.onKeyDown(keyCode,keyEvent);

    }
}
