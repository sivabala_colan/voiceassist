package com.newvoicesmart.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.Util;

public class AppPurchaseActivity extends BaseActivity {

    private Button mBTNBuyNow;
    private Activity mActivity;
    private ProgressDialog pDialog;
    private TextView mTVInAppPurchase,mTVJustOnly,mTVTwoDollar,mTVPerMonth;
     public static final String TAG = AppPurchaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_purchase);

        //INITIALIZE VIEW
        initializeView();

        //FONT TYPE
        fontType();

        mBTNBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.redirectFinishPage(mActivity,LoginActivity.class);

            }
        });
    }

    private void initializeView() {
        mActivity = AppPurchaseActivity.this;
        mBTNBuyNow = findViewById(R.id.btn_buy_now);
        mTVInAppPurchase = findViewById(R.id.txt_in_app_purchase);
        mTVJustOnly = findViewById(R.id.txt_just_only);
        mTVTwoDollar = findViewById(R.id.txt_two_dollar);
        mTVPerMonth = findViewById(R.id.txt_per_month);

    }
    //SET FONT FOR VIEW
    private void fontType(){
        Util.setFont(1,mActivity,mTVInAppPurchase,getString(R.string.in_app_purchase));
        Util.setFont(1,mActivity,mTVJustOnly,getString(R.string.just_only));
        Util.setFont(1,mActivity,mTVTwoDollar,getString(R.string.two_doller));
        Util.setFont(1,mActivity,mTVPerMonth,getString(R.string.per_month));
        Util.setFont(2,mActivity,mBTNBuyNow,getString(R.string.buy_now));


    }

    public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            Util.redirectFinishPage(mActivity,InformationActivity.class);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return  super.onKeyDown(keyCode,keyEvent);

    }
}
