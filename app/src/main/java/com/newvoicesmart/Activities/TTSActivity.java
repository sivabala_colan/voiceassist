package com.newvoicesmart.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Process;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.*;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.newvoicesmart.Handler.ExceptionHandler;
import com.newvoicesmart.Models.SMSModel;
import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.AcceptCallActivity;
import com.newvoicesmart.Utilities.Constants;
import com.newvoicesmart.Utilities.Mail;
import com.newvoicesmart.Utilities.MessageDialogFragment;
import com.newvoicesmart.Utilities.NotificationCall;
import com.newvoicesmart.Utilities.SpeechService;
import com.newvoicesmart.Utilities.UserSessionManager;
import com.newvoicesmart.Utilities.VoiceRecorder;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

public class TTSActivity extends AppCompatActivity implements MessageDialogFragment.Listener, TextToSpeech.OnInitListener {

    private static final String TAG = "iSpeech SDK Sample";
    Context _context;
    String ttsMessage = "";
    String mStatus = "";
    static String message = "";
    static String phoneNumber = "";
    static String phoneNumberWithSpace = "";
    static String name = "";
    Context context = this;
    //SmsManager smsManager;
    String speechText;
    String contactName = "";
    ArrayList<String> phoneNumberArrayList = new ArrayList<String>();
    static String voiceTriggerType = "";
    private static TTSActivity mInstance;
    protected volatile boolean mIsListening = false;
    private boolean isFinishing = false;
    private boolean isSpeaking = false;
    public static boolean isIncoming = false;
    private static boolean isGoodBye = false;
    private String googleTtsPackage = "com.google.android.tts", picoPackage = "com.svox.pico";
    private static int nCount = 0;
    private String strCrash = "";
    private Intent i;
    private KeyguardManager keyguardManager;

    //New Boolean Variables
    Boolean isVoiceSmartReadyToSpeak = false, isYouWantToText = false, isYouWantToCall = false, isYouWantToReadMyText = false, isYouWantSendEmail = false;
    Boolean isYouWantToPhoneNumberRead = false, isYouWantToMessageRead = false, isYouWantToReply = false, isYouWantToSpeechText = false;
    Boolean isSendTextToContactPerson = false, isCallToContactPerson = false, isContainMultiple = false;

    StringBuffer sb;
    StringBuffer chooseString;

    ArrayList<SMSModel> SMSModelArrayList = new ArrayList<SMSModel>();
    ArrayList<String> contactIdArrayList = new ArrayList<String>();
    ArrayList<String> nameArrayList = new ArrayList<String>();
    int currentMessageCount = 0;

    String endName;
    String[] splitMessage;
    int splitValue = 0;
    Timer timer;
    String value = "";
    UserSessionManager session;

    //	Boolean isNewSMSAction = false;

    String regexStr = "[0-9]+";
    //    String regexStr = "^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";
    Boolean currentlyReadPhoneNumber = false;

    Boolean isiSpeechStart = false;
    SharedPreferences pref;
    MediaPlayer mpdiaPr;
    TextToSpeech t1;
    PowerManager.WakeLock fullWakeLock;
    PowerManager.WakeLock partialWakeLock;

    TelephonyManager manager;
    StatePhoneReceiver myPhoneStateListener;
    boolean callFromApp = false; // To control the call has been made from the application
    boolean callFromOffHook = false; // To control the change to idle state is from the app call

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final String FRAGMENT_MESSAGE_DIALOG = "message_dialog";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 1;
    private SpeechService mSpeechService;
    private VoiceRecorder mVoiceRecorder;
    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        @Override
        public void onVoiceStart() {
            if (mSpeechService != null) {
                mSpeechService.startRecognizing(mVoiceRecorder.getSampleRate());
            }
        }

        @Override
        public void onVoice(byte[] data, int size) {
            if (mSpeechService != null) {
                mSpeechService.recognize(data, size);
            }
        }

        @Override
        public void onVoiceEnd() {
            if (mSpeechService != null) {
                mSpeechService.finishRecognizing();
            }
        }

    };

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            mSpeechService = SpeechService.from(binder);
            mSpeechService.addListener(mSpeechServiceListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mSpeechService = null;
        }

    };

    public static boolean isCallStarted = false;
    public CountDownTimer countDownTimer;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onCreate(Bundle savedInstanceState) {

        Window window1 = this.getWindow();
        window1.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window1.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window1.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        super.onCreate(savedInstanceState);
        _context = this.getApplicationContext();
        mInstance = this;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);//Test
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.tts);

        keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);

        session = new UserSessionManager(this);

        i = getIntent();
        if (i.hasExtra("crash")) {
            strCrash = i.getStringExtra("crash");
        }

        Log.e("Lock Status : ", keyguardManager.isKeyguardLocked() + "isKeyguardSecure : " + keyguardManager.isKeyguardSecure());

        //Lollipop && Marshmallow

        if (keyguardManager.isKeyguardLocked() || keyguardManager.isKeyguardSecure()) {

            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                Window window = this.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
                window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //Oreo
                createWakeLocks();
                wakeDevice();
            }

        }

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        //To be notified of changes of the phone state create an instance
        //of the TelephonyManager class and the StatePhoneReceiver class
        myPhoneStateListener = new StatePhoneReceiver(this);
        manager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));

        /*
        if (!keyguardManager.isKeyguardLocked() || Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {

            if (t1 == null) {
                t1 = new TextToSpeech(this, this, googleTtsPackage);
            } else {
                t1.stop();
                t1.shutdown();
                t1 = null;
            }

        }
        */
        if (t1 == null) {
            t1 = new TextToSpeech(this, this, googleTtsPackage);
        } else {

        }


        mIsListening = true;
//        if (getIntent().hasExtra("Close")) {
//            value = getIntent().getExtras().getString("Close");
//            if (value.equalsIgnoreCase("2")) {
//                reStartService();
//            }
//        }
//              else {
//                checkEndCall();
//            }
//        } else if (mIsListening) {
//            checkEndCall();
//        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        isCallStarted = false;
//        stopSpeechService();

        // Prepare Cloud Speech API
        bindService(new Intent(this, SpeechService.class), mServiceConnection, BIND_AUTO_CREATE);

        int PERMISSION_ALL = 1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NOTIFICATION_POLICY};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            } else {
                Log.d("Test Tag", "Test Message");
                //startVoiceRecorder();
                startVoiceRecorderInitially();
            }
        } else {
            Log.d("Test Tag", "Test Message");
            //startVoiceRecorder();
            startVoiceRecorderInitially();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

//        Window window = this.getWindow();
//        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
//        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
//        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        if (keyguardManager.isKeyguardLocked()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //Oreo
                if (fullWakeLock.isHeld()) {
                    fullWakeLock.release();
                }
                if (partialWakeLock.isHeld()) {
                    partialWakeLock.release();
                }
            }

            /*
            if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
                if (t1 == null) {
                    t1 = new TextToSpeech(this, this, googleTtsPackage);
                } else {
                    t1.stop();
                    t1.shutdown();
                    t1 = null;
                }
            }
            */

        }

        if (t1 == null) {
            t1 = new TextToSpeech(this, this, googleTtsPackage);
        } else {

        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        if(countDownTimer != null)
            countDownTimer.cancel();

        // Stop listening to voice
        Thread recordInBackGround= new Thread(new Runnable() {
            @Override
            public void run() {
                stopVoiceRecorderInitially();
                if (mSpeechService != null) {
                    // Stop Cloud Speech API
                    if(mServiceConnection != null){
                        mSpeechService.removeListener(mSpeechServiceListener);
                        unbindService(mServiceConnection);
                        mSpeechService = null;
                    }
                }
            }
        });
        recordInBackGround.start();




        if (t1 != null) {
            t1.stop();
            t1.shutdown();
            t1 = null;
        }

        if (isCallStarted) {
            Process.killProcess(Process.myPid());
        }

//        startSpeechService();

    }

    public void callTTS() {

        if (!isiSpeechStart) {
            stopVoiceRecorder();
        }
        if (Constants.haveNetworkConnection(TTSActivity.this)) {
            splitMessage = ttsMessage.split("\\|");

            if (splitMessage.length > 1) {
                timer = new Timer();
                if (splitValue == 0) {//At this line a new Thread will be created
                    timer.schedule(new RemindTask(), 0);
                } else {
                    timer.schedule(new RemindTask(), 10);
                }
            } else {
                timer = new Timer();  //At this line a new Thread will be created
                timer.schedule(new RemindTask(), 0);
            }
        } else {
            mpdiaPr = MediaPlayer.create(TTSActivity.this, R.raw.networkconnection);
            if (!mpdiaPr.isPlaying()) {
                mpdiaPr.start();
                mpdiaPr.setOnCompletionListener(new OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        finish();
                    }
                });
            }
        }
    }

    @Override
    public void onMessageDialogDismissed() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                REQUEST_RECORD_AUDIO_PERMISSION);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result;
            String language = Constants.languageCode();
            result = t1.setLanguage(new Locale(language));

            int ttsListener = t1.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.e("onStart", "TTS Engine Start");

                    if (mSpeechService != null) {
                        // Stop listening to voice
                        //stopVoiceRecorder();
                        // Stop Cloud Speech API
//                        mSpeechService.removeListener(mSpeechServiceListener);
//                        unbindService(mServiceConnection);
//                        mSpeechService = null;
                    }

                }

                @Override
                public void onDone(String utteranceId) {
                    Log.e("onDone", "TTS Engine Done");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (isiSpeechStart) {
                                mIsListening = false;
                                if (splitMessage.length > 0) {
                                    if (splitMessage.length == 1) {

                                        startSpeechRecognition();
                                        isSpeaking = true;

                                        if (isFinishing && !isIncoming && !isGoodBye) {
                                            recreateLoop();
                                        } else if (isFinishing && isIncoming) {

                                            reStartService();
                                            Process.killProcess(Process.myPid());

                                        } else if (isFinishing && isGoodBye) {

                                            reStartService();
                                            Process.killProcess(Process.myPid());

                                        }


                                    } else if (splitMessage.length > 1) {
                                        if (splitValue == 0) {
                                            callTTS();
                                        } else {
                                            if (splitValue < splitMessage.length) {
                                                callTTS();
                                            } else {
                                                splitValue = 0;
                                                startSpeechRecognition();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                }

                @Override
                public void onError(String utteranceId) {
                    Log.e("onError", "TTS Engine Error");
                }
            });
            if (ttsListener != TextToSpeech.SUCCESS) {
                Log.e(TAG, "failed to add utterance progress listener");
            }

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                Log.i("TTS", "This Language is supported");
                if (getIntent().hasExtra("Close")) {
                    value = getIntent().getExtras().getString("Close");
                    if (value.equalsIgnoreCase("2")) {
                        reStartService();
//                        isCallFinished = true;
                        Process.killProcess(Process.myPid());//may be we have to hide
                    } else {
                        checkEndCall();
                    }
                } else if (mIsListening) {
                    checkEndCall();
                }
            }

        } else if (status == TextToSpeech.ERROR) {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    public void recreateLoop(){
        mIsListening = true;
        isFinishing = false;
        ttsMessage = getResources().getString(R.string.text_or_call1);
        initializeVariables(true, false, false, false, false, false, false, false, false, false, false);
        callTTS();
        currentlyReadPhoneNumber = false;
        voiceTriggerType = "0";
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {

            if (Constants.haveNetworkConnection(TTSActivity.this)) {
                if (t1 != null) {
                    isiSpeechStart = true;
//                    synthesis.setStreamType(AudioManager.STREAM_MUSIC);
                    try {
                        if (splitMessage.length > 0) {
                            if (splitMessage.length == 1) {
//                                synthesis.speak(splitMessage[0]);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    t1.speak(splitMessage[0], TextToSpeech.QUEUE_FLUSH, null, "ID");
                                } else {
                                    t1.speak(splitMessage[0], TextToSpeech.QUEUE_FLUSH, null);
                                }
                            }
                            if (splitMessage.length > 1) {
                                if (splitValue == 0) {
//                                    synthesis.speak(splitMessage[0]);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        t1.speak(splitMessage[0], TextToSpeech.QUEUE_FLUSH, null, "ID");
                                    } else {
                                        t1.speak(splitMessage[0], TextToSpeech.QUEUE_FLUSH, null);
                                    }
                                    splitValue++;
                                } else {
//                                    synthesis.speak(splitMessage[splitValue]);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        t1.speak(splitMessage[splitValue], TextToSpeech.QUEUE_ADD, null, "ID");
                                    } else {
                                        t1.speak(splitMessage[splitValue], TextToSpeech.QUEUE_ADD, null);
                                    }
                                    if (splitValue < splitMessage.length) {
                                        splitValue++;
                                    } else {
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Network is not available\n" + e.getMessage());
                    }
                } else {

                }
            } else {
                //Toast.makeText(getApplicationContext(), "Please check your Network connection", Toast.LENGTH_SHORT).show();
            }

            timer.cancel();
//            onPlaySuccessful();
        }
    }

    //New
    public void initializeVariables(Boolean value1, Boolean value2, Boolean value3, Boolean value4, Boolean value5, Boolean value6, Boolean value7, Boolean value8, Boolean value9, Boolean value10, Boolean value11) {
        isVoiceSmartReadyToSpeak = value1;
        isYouWantToText = value2;
        isYouWantToCall = value3;
        isYouWantToReadMyText = value4;
        isYouWantToPhoneNumberRead = value5;
        isYouWantToMessageRead = value6;
        isYouWantToReply = value7;
        isYouWantToSpeechText = value8;
        isSendTextToContactPerson = value9;
        isCallToContactPerson = value10;
        isYouWantSendEmail = value11;
    }

    public ArrayList<SMSModel> getAllSms() {
        ArrayList<SMSModel> lstSms = new ArrayList<SMSModel>();
        SMSModel objSMSModel = new SMSModel();
        Uri message = Uri.parse("content://sms/inbox");
        ContentResolver cr = this.getContentResolver();
        java.util.GregorianCalendar gCal = new GregorianCalendar();
        // get how many previous days text you want
        gCal.add(Calendar.DATE, -30);
        // get yesterday's date in milliseconds
        String numberOfDaysTextYouWant = String.valueOf(gCal.getTime().getTime());
        // Fetch Inbox SMS Message from Built-in Content Provider
        Cursor c = cr.query(message, null, "date > ?", new String[]{numberOfDaysTextYouWant}, null);

        this.startManagingCursor(c);
        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                objSMSModel = new SMSModel();
                objSMSModel.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                objSMSModel.setAddress(c.getString(c.getColumnIndexOrThrow("address")));
                objSMSModel.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                objSMSModel.setReadState(c.getString(c.getColumnIndex("read")));
                objSMSModel.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    objSMSModel.setFolderName("inbox");
                } else {
                    objSMSModel.setFolderName("sent");
                }

                lstSms.add(objSMSModel);
                c.moveToNext();
            }
        }
        return lstSms;
    }

    public static String getDate(String time) {
        BigDecimal ll = new BigDecimal(time);
        DateFormat formatter = new SimpleDateFormat("yyyy MMMM dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ll.longValue());
        return formatter.format(calendar.getTime());
    }

    public static String getTime(String time) {
        BigDecimal ll = new BigDecimal(time);
        DateFormat formatter = new SimpleDateFormat("hh:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ll.longValue());
        String tt = formatter.format(calendar.getTime());
        return tt;
    }

    //New
    public void removeSpecialCharFromPhoneNumber() {
        // TODO Auto-generated method stub
        if (phoneNumber.contains("(")) {
            phoneNumber = phoneNumber.replace('(', ' ');
        }
        if (phoneNumber.contains(")")) {
            phoneNumber = phoneNumber.replace(')', ' ');
        }
        if (phoneNumber.contains("-")) {
            phoneNumber = phoneNumber.replace('-', ' ');
        }
        if (phoneNumber.contains(" ")) {
            phoneNumber = phoneNumber.replaceAll(" ", "");
        }
    }

    //New
    //Get Contact name
    private String getContactName(Context context, String number) {
        String name = null;
        try {
            String[] projection = new String[]{
                    ContactsContract.PhoneLookup.DISPLAY_NAME,
                    ContactsContract.PhoneLookup._ID};
            Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                }
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return name;
    }

    //New
    //Get Phone number
    private void getPhoneNumber(Context context, String name) {
        try {
            ContentResolver cr = getContentResolver();
            String[] arrayString = name.split("\\s+");

            if (arrayString.length == 2) {
                if (arrayString[0].length() == 1) {
                    if (arrayString[1].length() > 1) {
                        arrayString[0] = arrayString[1];
                        arrayString[1] = "";
                    }
                }
            } else if (arrayString.length == 3) {
                if (arrayString[0].length() == 1) {
                    if (arrayString[1].length() > 1) {
                        arrayString[0] = arrayString[1];
                        arrayString[1] = "";
                    } else if (arrayString[2].length() > 1) {
                        arrayString[0] = arrayString[2];
                        arrayString[1] = "";
                        arrayString[2] = "";
                    }
                } else if (arrayString[1].length() == 1) {
                    if (arrayString[2].length() > 1) {
                        arrayString[1] = arrayString[2];
                        arrayString[2] = "";
                    }
                }
            } else if (arrayString.length == 4) {
                if (arrayString[0].length() == 1) {
                    if (arrayString[1].length() > 1) {
                        arrayString[0] = arrayString[1];
                        arrayString[1] = "";
                    } else if (arrayString[2].length() > 1) {
                        arrayString[0] = arrayString[2];
                        arrayString[1] = "";
                        arrayString[2] = "";
                    } else if (arrayString[3].length() > 1) {
                        arrayString[0] = arrayString[3];
                        arrayString[1] = "";
                        arrayString[2] = "";
                        arrayString[3] = "";
                    }
                } else if (arrayString[1].length() == 1) {
                    if (arrayString[2].length() > 1) {
                        arrayString[1] = arrayString[2];
                        arrayString[2] = "";
                    } else if (arrayString[3].length() > 1) {
                        arrayString[1] = arrayString[3];
                        arrayString[2] = "";
                        arrayString[3] = "";
                    }
                } else if (arrayString[2].length() == 1) {
                    if (arrayString[3].length() > 1) {
                        arrayString[2] = arrayString[3];
                        arrayString[3] = "";
                    }
                }
            }
            Cursor cursor = null;

            String selection1 = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + ("1") + "'";
            String selection2 = "lower(" + ContactsContract.Contacts.DISPLAY_NAME + ")=lower('" + name + "')";


            //			String selection3 = ContactsContract.Contacts.DISPLAY_NAME + " = '"+ name +"'";
            //			String selection4 = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + ("1") + "'";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
            } else {
                cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, selection1 + " AND " + selection2, null, null);
                if (cursor.getCount() == 0) {
                    cursor = cr.query(
                            ContactsContract.Contacts.CONTENT_URI,
                            null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                            new String[]{arrayString[0] + "%"},
                            null);
                    if (cursor.getCount() == 0) {
                        cursor = cr.query(
                                ContactsContract.Contacts.CONTENT_URI,
                                null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                                new String[]{"%" + arrayString[0]},
                                null);
                        if (cursor.getCount() == 0) {
                            cursor = cr.query(
                                    ContactsContract.Contacts.CONTENT_URI,
                                    null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                                    new String[]{arrayString[1] + "%"},
                                    null);

                            if (cursor.getCount() == 0) {
                                cursor = cr.query(
                                        ContactsContract.Contacts.CONTENT_URI,
                                        null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                                        new String[]{"%" + arrayString[1]},
                                        null);
                                if (cursor.getCount() == 0) {
                                    cursor = cr.query(
                                            ContactsContract.Contacts.CONTENT_URI,
                                            null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                                            new String[]{"%" + arrayString[0] + "%"},
                                            null);
                                    if (cursor.getCount() == 0) {
                                        cursor = cr.query(
                                                ContactsContract.Contacts.CONTENT_URI,
                                                null, "LOWER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ?",
                                                new String[]{"%" + arrayString[1] + "%"},
                                                null);
                                    }
                                }
                            }
                        }
                    }
                }

                if (contactIdArrayList.size() > 0) {
                    contactIdArrayList.clear();
                }
                if (nameArrayList.size() > 0) {
                    nameArrayList.clear();
                }

                if (cursor.moveToFirst()) {
                    do {
                        contactIdArrayList.add(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID)));
                        nameArrayList.add(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            Log.e("getPhoneNumber", e.getMessage());
        }
    }

    public ArrayList<String> getCurrentPhoneNumber(String contactId) {
        String mobile_number = "";
        String home_number = "";
        String work_number = "";
        String other_number = "";
        String custom_number = "";
        ContentResolver cr = getContentResolver();
        Cursor phones = cr.query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = " + contactId, null, null);

        if (phoneNumberArrayList.size() > 0) {
            phoneNumberArrayList.clear();
        }

        while (phones.moveToNext()) {
            String number = phones.getString(phones.getColumnIndex(Phone.NUMBER));
            int type = phones.getInt(phones.getColumnIndex(Phone.TYPE));
            switch (type) {
                case Phone.TYPE_MOBILE:
                    Log.e("TYPE_MOBILE ===>", "" + number);
                    mobile_number = number;
                    break;
                case Phone.TYPE_HOME:
                    Log.e("TYPE_HOME ==> ", "" + number);
                    home_number = number;
                    break;
                case Phone.TYPE_WORK:
                    Log.e("TYPE_WORK ====>", "" + number);
                    work_number = number;
                    break;
                case Phone.TYPE_OTHER:
                    Log.e("TYPE_OTHER ====>", "" + number);
                    other_number = number;
                    break;
                case Phone.TYPE_CUSTOM:
                    Log.e("TYPE_OTHER ====>", "" + number);
                    custom_number = number;
                    break;
            }
        }
        if (mobile_number != null && mobile_number.length() > 0) {
            phoneNumberArrayList.add(mobile_number);
        } else if (home_number != null && home_number.length() > 0) {
            phoneNumberArrayList.add(home_number);
        } else if (work_number != null && work_number.length() > 0) {
            phoneNumberArrayList.add(work_number);
        } else if (other_number != null && other_number.length() > 0) {
            phoneNumberArrayList.add(other_number);
        } else if (custom_number != null && custom_number.length() > 0) {
            phoneNumberArrayList.add(custom_number);
        }
        phones.close();
        return phoneNumberArrayList;
    }

    public void callMultiple(int value, String type) {
        if (contactIdArrayList.size() > (value - 1)) {
            ArrayList<String> phone = new ArrayList<String>();
            phone = getCurrentPhoneNumber(contactIdArrayList.get(value));
            if (phone.size() > 0) {
                phoneNumber = phone.get(0);
                removeSpecialCharFromPhoneNumber();
                name = contactName;

                if (type.equalsIgnoreCase("call")) {
                    isCallToContactPerson = true;
                } else {
                    isSendTextToContactPerson = true;
                }

                mIsListening = true;
                Log.e("Contact Name", nameArrayList.get(value));
                ttsMessage = getResources().getString(R.string.you_said) + nameArrayList.get(value) + getResources().getString(R.string.correct_please_confirm);
            } else {
                mIsListening = true;
                ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
            }

        } else {
            mIsListening = true;
            ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
        }

        currentlyReadPhoneNumber = false;
        callTTS();
    }

    @TargetApi(23)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkEndCall() {

        if (getIntent().hasExtra("KEY")) {
            mIsListening = true;
            if (nCount == 0 && strCrash.equalsIgnoreCase(""))
                ttsMessage = getResources().getString(R.string.text_or_call);
            else
                ttsMessage = getResources().getString(R.string.text_or_call1);
            //			isNewSMSAction = false;
            initializeVariables(true, false, false, false, false, false, false, false, false, false, false);
            callTTS();
            currentlyReadPhoneNumber = false;
            voiceTriggerType = "0";
        } else {
            if (getIntent().hasExtra("isCallKey")) {
                // Start service from Receiver
                voiceTriggerType = getIntent().getExtras().getString("isCallKey");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    //mNotificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_PRIORITY);

                    if (mNotificationManager.isNotificationPolicyAccessGranted()) {
                        actionForIncomingreceiver();
                    } else {
                        // Ask the user to grant access
                        Intent i = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                        startActivityForResult(i, 0);
                    }
                } else {

                    actionForIncomingreceiver();

                }
            }
        }
    }

    public void actionForIncomingreceiver() {
        isIncoming = true;
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
        phoneNumber = getIntent().getExtras().getString("phoneNumber");
        message = getIntent().getExtras().getString("message");
        name = getContactName(context, phoneNumber);

        removeSpecialCharFromPhoneNumber();
        //				isNewSMSAction = false;

        // Start service from SMS Receiver
        mIsListening = true;
        if (voiceTriggerType.equalsIgnoreCase("0")) {
            isYouWantToPhoneNumberRead = true;
            if (name != null) {
                ttsMessage = getResources().getString(R.string.text_from) + name + getResources().getString(R.string.read_message_ignore);
                currentlyReadPhoneNumber = false;
            } else {
                phoneNumberMakeReadable();
                ttsMessage = getResources().getString(R.string.text_from) + phoneNumberWithSpace + getResources().getString(R.string.read_message_ignore);
                currentlyReadPhoneNumber = true;
            }
        }
        // Start service from Incoming Call Receiver
        else if (voiceTriggerType.equalsIgnoreCase("1")) {
            if (name != null) {
                ttsMessage = getResources().getString(R.string.call_from) + name + getResources().getString(R.string.reply_reject);
                currentlyReadPhoneNumber = false;
            } else {
                phoneNumberMakeReadable();
                ttsMessage = getResources().getString(R.string.call_from) + phoneNumberWithSpace + getResources().getString(R.string.reply_reject);
                currentlyReadPhoneNumber = true;
            }
        }
        callTTS();
    }

    //New
    public boolean isNumeric(String str) {
        if (str.contains("(")) {
            str = str.replace('(', ' ');
        }
        if (str.contains(")")) {
            str = str.replace(')', ' ');
        }
        if (str.contains("-")) {
            str = str.replace('-', ' ');
        }
        if (str.contains(" ")) {
            str = str.replaceAll(" ", "");
        }
        //		Log.d("Phone Number str = ", ""+str);
        if (str.matches(regexStr)) {
            return true;
        } else {
            return false;
        }
    }

    //New
    public void phoneNumberMakeReadable() {
        if (phoneNumber.length() > 1) {
            phoneNumberWithSpace = "";
            for (int i = 0; i < phoneNumber.length(); i++) {
                phoneNumberWithSpace += phoneNumber.charAt(i); //  add the char at the next index
                phoneNumberWithSpace += " "; //then add the space
                if (phoneNumber.length() > 7) {
                    if (i == 2 || i == 5) {
                        phoneNumberWithSpace += "|"; //then add the space
                    }
                }
            }
            //			Log.d("phoneNumberWithSpace =  ", phoneNumberWithSpace);
        }
    }

    //New
    private void answerPhoneHeadsethook(final Context context) {

        // froyo and beyond trigger on buttonUp instead of buttonDown
        Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
        buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");

        // Simulate a press of the head set button to pick up the call
        Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);
        buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");

        reStartService();

    }

    //New
    private void answerPhoneAidl(Context context) throws Exception {
        // Set up communication with the telephony service
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Class c = Class.forName(tm.getClass().getName());
        Method m = c.getDeclaredMethod("getITelephony");
        m.setAccessible(true);
        ITelephony telephonyService;
        telephonyService = (ITelephony) m.invoke(tm);

        // Silence the ringer and answer the call!adb kill-server
        telephonyService.silenceRinger();
        isCallStarted = true;

        stopVoiceRecorder();

        if (Build.VERSION.SDK_INT >= 23) {
            String enabledNotificationListeners = Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners");
            if (enabledNotificationListeners != null && enabledNotificationListeners.contains(getApplicationContext().getPackageName())) {
                try {
                    for (MediaController mediaController : ((MediaSessionManager) getApplicationContext().getSystemService(Context.MEDIA_SESSION_SERVICE)).getActiveSessions(new ComponentName(getApplicationContext(), NotificationCall.class))) {
                        if ("com.android.server.telecom".equals(mediaController.getPackageName())) {
//                            mediaController.dispatchMediaButtonEvent(new KeyEvent(1, 79));
                            if (Build.VERSION.SDK_INT >= 23 && Build.VERSION.SDK_INT <= 25) {
                                mediaController.dispatchMediaButtonEvent(new KeyEvent(1, 79));
                            } else if (Build.VERSION.SDK_INT >= 26 && Build.VERSION.SDK_INT <= 27) {
                                mediaController.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
                                mediaController.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
                            }
                            return;
                        }
                    }
                } catch (SecurityException e2) {
                    e2.printStackTrace();
                }
            } else {
                Intent i = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

        } else if (Build.VERSION.SDK_INT >= 21) {
            Intent answerCalintent = new Intent(context, AcceptCallActivity.class);
            answerCalintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            answerCalintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(answerCalintent);
        } else {
            Intent intent = new Intent(context, AcceptCallActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
//        reStartService();//It may comes before attend the call in nougat
    }

    //New
    private void endCall() {
        mIsListening = true;
        ttsMessage = getResources().getString(R.string.call_ignored);
        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
        callTTS();

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            com.android.internal.telephony.ITelephony telephonyService = (ITelephony) m.invoke(tm);

            telephonyService.endCall();
//            reStartService();
            isFinishing = true;
        } catch (Exception e) {
            System.out.println("error on end call : " + e.getMessage());
            e.printStackTrace();
        }
    }

    //New
    //Sends an SMS message
    private void sendSMS(String phoneNumber, String message) {

        try {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            final SMSModel SMSModelObject = new SMSModel();
            SMSModelObject.setAddress(phoneNumber);
            SMSModelObject.setMsg(message);
            SMSModelObject.setTime("" + System.currentTimeMillis());
            SMSModelObject.setReadState("1");

            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> messageParts = smsManager.divideMessage(message);
            int partsCount = messageParts.size();

            ArrayList<PendingIntent> sentPendings = new ArrayList<PendingIntent>(partsCount);
            ArrayList<PendingIntent> deliveredPendings = new ArrayList<PendingIntent>(partsCount);

            for (int i = 0; i < partsCount; i++) {

                /* Adding Sent PendingIntent For Message Part */

                PendingIntent sentPending = PendingIntent.getBroadcast(this,
                        0, new Intent("SENT"), 0);

                this.registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context arg0, Intent arg1) {
                        AudioManager audioManager;
                        switch (getResultCode()) {
                            case Activity.RESULT_OK:
                                audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                                showToastMessage("sent message");
                                mIsListening = true;
                                ttsMessage = "" + getResources().getString(R.string.text_sent);
                                currentlyReadPhoneNumber = false;
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                                callTTS();
                                restoreSms(SMSModelObject, "success");
                                isFinishing = true;
//                            reStartService();

//                            startActivity(new Intent(context, MainActivity.class));
                                break;
                            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                                showToastMessage("Generic failure");
                                mIsListening = true;
                                ttsMessage = "" + getResources().getString(R.string.text_not_sent_generic);
                                currentlyReadPhoneNumber = false;
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                                callTTS();
                                restoreSms(SMSModelObject, "failed");
                                isFinishing = true;
//                            reStartService();
                                break;
                            case SmsManager.RESULT_ERROR_NO_SERVICE:
                                showToastMessage("No service");
                                mIsListening = true;
                                ttsMessage = "" + getResources().getString(R.string.text_not_sent_service);
                                currentlyReadPhoneNumber = false;
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                                callTTS();
                                restoreSms(SMSModelObject, "success");
                                isFinishing = true;
//                            reStartService();
                                break;
                            case SmsManager.RESULT_ERROR_NULL_PDU:
                                showToastMessage("Null PDU");
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                                break;
                            case SmsManager.RESULT_ERROR_RADIO_OFF:
                                showToastMessage("Radio off");
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                                break;
                            default:
                                initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        }
                        TTSActivity.this.unregisterReceiver(this);
                    }
                }, new IntentFilter("SENT"));

                sentPendings.add(sentPending);

                /* Adding Delivered PendingIntent For Message Part */

                PendingIntent deliveredPending = PendingIntent.getBroadcast(
                        this, 0, new Intent("DELIVERED"), 0);

                this.registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context arg0, Intent arg1) {
                        switch (getResultCode()) {
                            case Activity.RESULT_OK:
                                showToastMessage("SMS delivered");
                                break;
                            case Activity.RESULT_CANCELED:
                                showToastMessage("SMS not delivered");
                                break;
                        }
                        TTSActivity.this.unregisterReceiver(this);
                    }
                }, new IntentFilter("DELIVERED"));

                deliveredPendings.add(deliveredPending);
            }

            smsManager.sendMultipartTextMessage(phoneNumber, null, messageParts, sentPendings, deliveredPendings);

        } catch (Exception e) {
            Log.e("sendSMS", e.getMessage());
        }

    }

    // New
    public boolean restoreSms(SMSModel obj, String type) {
        boolean ret = false;
        try {
            ContentValues values = new ContentValues();
            values.put("address", obj.getAddress());
            values.put("body", obj.getMsg());
            values.put("read", obj.getReadState());

            String storeLocation = "sent";
            if (type.equalsIgnoreCase("success")) {
                storeLocation = "sent";
            } else {
                storeLocation = "draft";
            }
            values.put("date", obj.getTime());
            if (obj.getFolderName() != null)
                Log.d("FolderName : ß", "" + obj.getFolderName());
            this.getContentResolver().insert(Uri.parse("content://sms/" + storeLocation), values);
            ret = true;
        } catch (Exception ex) {
            ret = false;
        }
        return ret;
    }

    //New
    void showToastMessage(String message) {
        //Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void startSpeechRecognition() {
        // Prepare Cloud Speech API
//        bindService(new Intent(this, SpeechService.class), mServiceConnection, BIND_AUTO_CREATE);
        isiSpeechStart = false;
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NOTIFICATION_POLICY};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            startVoiceRecorder();
        }

    }


    public void reStartService() {
        try {
            stopVoiceRecorder();
            pref = getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
            Editor mEditor = pref.edit();
            mEditor.putString("isIncomingStart", "no");
            mEditor.commit();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Exception:" + e.toString());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (keyguardManager.isKeyguardLocked()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //Oreo
                partialWakeLock.acquire();
            }

        }


    }

    public void startSpeechService() {
//        if (!isCallStarted && !VoiceTriggerService.isVoiceTriggerRunning)
//            startService(new Intent(this, VoiceTriggerService.class));
    }

    public void stopSpeechService() {
//        if (VoiceTriggerService.isVoiceTriggerRunning)
//            stopService(new Intent(this, VoiceTriggerService.class));
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        reStartService();
        Process.killProcess(Process.myPid());
//        startActivity(new Intent(this,MainActivity.class));
    }


    public static boolean closeActivity() {
        if (mInstance != null) {
            try{
                mInstance.finish();
                return true;
            } catch (Exception e){};
        }
        return false;
    }

    // Monitor for changes to the state of the phone
    public class StatePhoneReceiver extends PhoneStateListener {
        Context context;

        public StatePhoneReceiver(Context context) {
            this.context = context;
        }

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state) {

                case TelephonyManager.CALL_STATE_OFFHOOK: //Call is established
                    if (callFromApp) {
                        callFromApp = false;
                        callFromOffHook = true;

                        try {
                            sleep(500); // Delay 0,5 seconds to handle better turning on loudspeaker
                        } catch (InterruptedException e) {
                        }

                        //Activate loudspeaker
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setMode(AudioManager.MODE_IN_CALL);

                        if (!audioManager.isWiredHeadsetOn())
                            audioManager.setSpeakerphoneOn(true);
                    }
                    break;

                case TelephonyManager.CALL_STATE_IDLE: //Call is finished
                    if (callFromOffHook) {
                        callFromOffHook = false;
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setMode(AudioManager.MODE_NORMAL); //Deactivate loudspeaker
                        manager.listen(myPhoneStateListener, // Remove listener
                                PhoneStateListener.LISTEN_NONE);
                        isCallStarted = false;
//                        reStartService();//Test

                        session.createCrashSession(false);

                        if (isFinishing && !isIncoming) {
                            recreateLoop();
                        } else if (isFinishing && isIncoming) {
                            reStartService();
                        }

//                        startSpeechService();
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            if (permissions.length == 6 && grantResults.length == 6
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED && grantResults[5] == PackageManager.PERMISSION_GRANTED) {
                startVoiceRecorderInitially();
            } else {
                showPermissionMessageDialog();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private final SpeechService.Listener mSpeechServiceListener =
            new SpeechService.Listener() {
                @Override
                public void onSpeechRecognized(final String text, final boolean isFinal) {
                    if (isFinal) {
                        mVoiceRecorder.dismiss();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isFinal) {
                                speechResponse(text);
                            } else {
//                                showToastMessage("Nothing");
                            }

                        }
                    });
                }
            };

    private void startVoiceRecorder() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.start();
        }else {
            try {
                mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
                mVoiceRecorder.start();
            } catch (Exception e) {
                Log.e("Exception found", e.getMessage());
            }
        }
        if(ttsMessage.contains(getResources().getString(R.string.call_from))){
            countDownTimer = new CountDownTimer(7000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    String isIncomingAnsweredValue = "";
                    pref = getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
                    if(pref.contains("isIncomingAnswered"))
                        isIncomingAnsweredValue = pref.getString("isIncomingAnswered", "");
                    if(isIncomingAnsweredValue != null){
                        if(ttsMessage.contains(getResources().getString(R.string.call_from)) && isIncomingAnsweredValue.equalsIgnoreCase("No")){
                            mIsListening = true;
                            callTTS();
                        } else
                            countDownTimer.cancel();
                    }
                }
            }.start();
        }
    }

    private void startVoiceRecorderInitially() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
        }
        try {
            mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
        } catch (Exception e) {
            Log.e("Exception found", e.getMessage());
        }

    }

    private void stopVoiceRecorder() {

        try {
            if (mVoiceRecorder != null) {
                mVoiceRecorder.stopForTTS();
            }
        } catch (Exception e) {
            Log.e("Stop Voice Recorder", e.getMessage());
        }


    }

    private void stopVoiceRecorderInitially() {

        try {
            if (mVoiceRecorder != null) {
                mVoiceRecorder.stopForTTS();
                mVoiceRecorder.stop();
                mVoiceRecorder = null;
            }
        } catch (Exception e) {
            Log.e("Stop Voice Recorder", e.getMessage());
        }


    }

    private void showPermissionMessageDialog() {
        MessageDialogFragment
                .newInstance(getString(R.string.permission_message))
                .show(getSupportFragmentManager(), FRAGMENT_MESSAGE_DIALOG);
    }

    private void speechResponse(String strResult) {
        if(countDownTimer != null)
        countDownTimer.cancel();
        mStatus = "Got some results";
        String cap = strResult.substring(0, 1).toUpperCase() + strResult.substring(1);
        Log.e("isNumeric", "Result: " + strResult + "-" + isNumeric(strResult));
        //Toast.makeText(context, strResult, Toast.LENGTH_SHORT).show();
        //			Log.d("Val 10 = ", "onResults " + results);

        if (strResult != null || strResult.equals("")) {
            Log.i("Val 11 = ", strResult);
//            showToastMessage(strResult);

            // Start service from Incoming Call Receiver
            if (voiceTriggerType.equalsIgnoreCase("1") && strResult != null && (strResult.equalsIgnoreCase(getResources().getString(R.string.answer)) || strResult.equalsIgnoreCase(getResources().getString(R.string.reply)) || strResult.equalsIgnoreCase(getResources().getString(R.string.alo)) || strResult.equalsIgnoreCase("có") || strResult.equalsIgnoreCase(getResources().getString(R.string.yes)))) {
                //						answerPhoneHeadsethook(getApplicationContext());
                manager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE); // start listening to the phone changes
                callFromApp = true;
                try {
                    answerPhoneAidl(context);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AutoAnswer", "Error trying to answer using telephony service.  Falling back to headset.");
                    answerPhoneHeadsethook(context);
                }

            } else if (voiceTriggerType.equalsIgnoreCase("1") && (strResult.equalsIgnoreCase(getResources().getString(R.string.repeat)) || strResult.equalsIgnoreCase("nói lại"))) {
                mIsListening = true;
                if (name != null) {
                    ttsMessage = getResources().getString(R.string.call_from) + name + getResources().getString(R.string.reply_reject);
                    currentlyReadPhoneNumber = false;
                } else {
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.call_from) + phoneNumberWithSpace + getResources().getString(R.string.reply_reject);
                    currentlyReadPhoneNumber = true;
                }
                callTTS();
            } else if ((voiceTriggerType.equalsIgnoreCase("1") && (strResult.equalsIgnoreCase(getResources().getString(R.string.refused))))
                    || (voiceTriggerType.equalsIgnoreCase("1") && (strResult.equalsIgnoreCase(getResources().getString(R.string.no))))
                    || (voiceTriggerType.equalsIgnoreCase("1") && (strResult.equalsIgnoreCase(getResources().getString(R.string.ignore))))
                    || (voiceTriggerType.equalsIgnoreCase("1") && (strResult.equalsIgnoreCase(getResources().getString(R.string.unheard))))) {
                Log.d("voiceTriggerType = 	", voiceTriggerType);
                endCall();
            } else if (voiceTriggerType.equalsIgnoreCase("1")) {
                mIsListening = true;
                if (name != null) {
                    ttsMessage = getResources().getString(R.string.call_from) + name + getResources().getString(R.string.reply_reject);
                    currentlyReadPhoneNumber = false;
                } else {
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.call_from) + phoneNumberWithSpace + getResources().getString(R.string.reply_reject);
                    currentlyReadPhoneNumber = true;
                }
                String isIncomingAnsweredValue = "";
                pref = getSharedPreferences("VoiceSmartPref", Context.MODE_PRIVATE);
                if(pref.contains("isIncomingAnswered"))
                    isIncomingAnsweredValue = pref.getString("isIncomingAnswered", "");
                if(isIncomingAnsweredValue != null) {
                    if (ttsMessage.contains(getResources().getString(R.string.call_from)) && isIncomingAnsweredValue.equalsIgnoreCase("No")) {
                        callTTS();
                    }
                }
            }

            // Start service from SMS Receiver / Main Activity
            else {
                // Read My Text Action Trigger
                Log.d("isYouWantToCall = ", "" + isYouWantToReadMyText);

                if ((isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && !isYouWantToCall && !isYouWantToText && (strResult.equalsIgnoreCase(getResources().getString(R.string.ignore))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && !isYouWantToCall && !isYouWantToText && (strResult.equalsIgnoreCase(getResources().getString(R.string.stop))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && !isYouWantToCall && !isYouWantToText && (strResult.equalsIgnoreCase(getResources().getString(R.string.good_bye))))) {

                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.good_bye);
                    Log.d("ttsMessage = ", "" + ttsMessage);

                    initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                    currentlyReadPhoneNumber = false;
                    callTTS();

                    isGoodBye = true;
                    isFinishing = true;
                } else if ((isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && (strResult.equalsIgnoreCase(getResources().getString(R.string.read_the_message))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && (strResult.equalsIgnoreCase(getResources().getString(R.string.read_the_news))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToReadMyText && (strResult.equalsIgnoreCase(getResources().getString(R.string.read))))) {
                    currentMessageCount = 0;
                    SMSModelArrayList = getAllSms();
                    mIsListening = true;
                    if (SMSModelArrayList.size() > currentMessageCount) {
                        Log.d("Address = ", "" + SMSModelArrayList.get(currentMessageCount).getAddress());

                        phoneNumber = SMSModelArrayList.get(currentMessageCount).getAddress();
                        name = getContactName(context, phoneNumber);
                        removeSpecialCharFromPhoneNumber();

                        if (name != null) {
                            ttsMessage = getResources().getString(R.string.first_message_from) + name + getResources().getString(R.string.split) + SMSModelArrayList.get(currentMessageCount).getMsg() + getResources().getString(R.string.reply_callback_next_exit);
                            ttsMessage = ttsMessage.replace("\n", " ");
                            currentlyReadPhoneNumber = false;
                        } else {
                            phoneNumberMakeReadable();
                            ttsMessage = getResources().getString(R.string.first_message_from) + phoneNumberWithSpace + getResources().getString(R.string.split) + SMSModelArrayList.get(currentMessageCount).getMsg() + getResources().getString(R.string.reply_callback_next_exit);
                            ttsMessage = ttsMessage.replace("\n", " ");
                            currentlyReadPhoneNumber = true;
                        }
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(true, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else {
                        ttsMessage = getResources().getString(R.string.no_text_inbox);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    }
                } else if (isVoiceSmartReadyToSpeak == true && isYouWantToReadMyText && !isYouWantToMessageRead) {
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.next)) || strResult.equalsIgnoreCase(getResources().getString(R.string.next_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.read_the_next_message))) {
                        currentMessageCount += 1;
                        mIsListening = true;
                        if (SMSModelArrayList.size() > currentMessageCount) {
                            Log.d("Address = ", "" + SMSModelArrayList.get(currentMessageCount).getAddress());
                            nCount = nCount + 1;
                            phoneNumber = SMSModelArrayList.get(currentMessageCount).getAddress();
                            name = getContactName(context, phoneNumber);
                            removeSpecialCharFromPhoneNumber();

                            if (name != null) {
                                ttsMessage = getResources().getString(R.string.next_message_from) + name + getResources().getString(R.string.split) + SMSModelArrayList.get(currentMessageCount).getMsg() + getResources().getString(R.string.reply_callback_next_exit);
                                ttsMessage = ttsMessage.replace("\n", " ");
                                currentlyReadPhoneNumber = false;
                            } else {
                                phoneNumberMakeReadable();
                                ttsMessage = getResources().getString(R.string.next_message_from) + phoneNumberWithSpace + getResources().getString(R.string.split) + SMSModelArrayList.get(currentMessageCount).getMsg() + getResources().getString(R.string.reply_callback_next_exit);
                                ttsMessage = ttsMessage.replace("\n", " ");
                                currentlyReadPhoneNumber = true;
                            }
                            Log.d("ttsMessage = ", "" + ttsMessage);

                            initializeVariables(true, false, false, true, false, false, false, false, false, false, false);
                            currentlyReadPhoneNumber = false;
                            callTTS();

                        } else {
                            ttsMessage = getResources().getString(R.string.no_text_inbox);
                            Log.d("ttsMessage = ", "" + ttsMessage);
                            initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                            currentlyReadPhoneNumber = false;
                            callTTS();
//                            reStartService();
                            isFinishing = true;
                        }
                    } else if (strResult.equalsIgnoreCase("exit") || strResult.equalsIgnoreCase("cancel") || strResult.equalsIgnoreCase("no")
                            || strResult.equalsIgnoreCase("cancel it") || strResult.equalsIgnoreCase("ignore") || strResult.equalsIgnoreCase("end")
                            || strResult.equalsIgnoreCase("stop") || strResult.equalsIgnoreCase("main menu")
                            || strResult.equalsIgnoreCase("lối thoát") || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("Không")
                            || strResult.equalsIgnoreCase("hủy nó đi") || strResult.equalsIgnoreCase("bỏ qua") || strResult.equalsIgnoreCase("kết thúc")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("thực đơn chính")) {
                        mIsListening = true;
                        //								isNewSMSAction = true;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        ttsMessage = "";
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                        isIncoming = false;
                        isGoodBye = false;

                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.call_back)) || strResult.equalsIgnoreCase(getResources().getString(R.string.call)) || strResult.equalsIgnoreCase(getResources().getString(R.string.call1)) || strResult.equalsIgnoreCase(getResources().getString(R.string.phone_call))) {
                        manager.listen(myPhoneStateListener,
                                PhoneStateListener.LISTEN_CALL_STATE); // start listening to the phone changes
                        callFromApp = true;
                        isCallStarted = true;

                        makeCall();
                        isFinishing = true;
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.reply)) || strResult.equalsIgnoreCase(getResources().getString(R.string.write_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.write_message1)) || strResult.equalsIgnoreCase(getResources().getString(R.string.message))) {
                        isYouWantToText = true;
                        isYouWantToPhoneNumberRead = true;
                        isYouWantToMessageRead = true;
                        isYouWantToReply = true;
                        isYouWantToSpeechText = false;
                        mIsListening = true;
                        ttsMessage = "" + getResources().getString(R.string.dictate_text);
                        currentlyReadPhoneNumber = false;

                        isSendTextToContactPerson = true;//

                        callTTS();
                    } else {
                        mIsListening = false;
                        startSpeechRecognition();
//								startVoiceRecognition();
                    }
                }

                // Call Action Trigger
                else if ((isVoiceSmartReadyToSpeak == true && !isYouWantToCall && (strResult.equalsIgnoreCase(getResources().getString(R.string.call)) || strResult.equalsIgnoreCase("gọi")))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToCall && (strResult.equalsIgnoreCase(getResources().getString(R.string.make_a_call)) || strResult.equalsIgnoreCase("gọi điện")))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToCall && (strResult.equalsIgnoreCase(getResources().getString(R.string.make_a_call_to)) || strResult.equalsIgnoreCase("gọi điện thoại")))) {
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.name_or_number);
                    initializeVariables(true, false, true, false, false, false, false, false, false, false, false);
                    currentlyReadPhoneNumber = false;
                    callTTS();
                }

                // Read Phone Number Before make a Call
                else if (isVoiceSmartReadyToSpeak == true && isYouWantToCall == true && isNumeric(strResult) && !isCallToContactPerson) {

                    isCallToContactPerson = true;
                    mIsListening = true;
                    phoneNumber = strResult.toString();
                    removeSpecialCharFromPhoneNumber();
                    //							Log.d("Phone Number = ", ""+phoneNumber);
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.you_said) + phoneNumberWithSpace + getResources().getString(R.string.correct_please_confirm) + getResources().getString(R.string.or) + getResources().getString(R.string.back);
                    currentlyReadPhoneNumber = true;
                    callTTS();
                }

                // Read Contact Name Before make a Call
                else if (isVoiceSmartReadyToSpeak == true && isYouWantToCall == true && !isNumeric(strResult.toString()) && !isCallToContactPerson && !isContainMultiple) {
                    contactName = strResult.toLowerCase();
                    getPhoneNumber(getApplicationContext(), contactName);
                    if (nameArrayList.size() > 0) {

                        if (contactIdArrayList.size() > 0) {

                            if (nameArrayList.size() == 1) {
                                ArrayList<String> phone = new ArrayList<String>();
                                phone = getCurrentPhoneNumber(contactIdArrayList.get(0));
                                if (phone.size() > 0) {
                                    phoneNumber = phone.get(0);
                                    removeSpecialCharFromPhoneNumber();
                                    name = contactName;

                                    isCallToContactPerson = true;
                                    mIsListening = true;
                                    ttsMessage = getResources().getString(R.string.you_said) + nameArrayList.get(0) + getResources().getString(R.string.correct_please_confirm) + getResources().getString(R.string.or) + getResources().getString(R.string.back);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                } else {
                                    mIsListening = true;
                                    ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                }
                            } else {
                                sb = new StringBuffer();

                                chooseString = new StringBuffer();
                                if (nameArrayList.size() > 5) {
                                    for (int noA = 0; noA < 5; noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                    }
                                    chooseString.append(getResources().getString(R.string.one_five));
                                } else {
                                    for (int noA = 0; noA < nameArrayList.size(); noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                        if (noA == 0) {
                                            chooseString.append(getResources().getString(R.string.one));
                                        } else if (noA == 1) {
                                            chooseString.append("||" + getResources().getString(R.string.two));
                                        } else if (noA == 2) {
                                            chooseString.append("||" + getResources().getString(R.string.three));
                                        } else if (noA == 3) {
                                            chooseString.append("||" + getResources().getString(R.string.four));
                                        } else if (noA == 4) {
                                            chooseString.append("||" + getResources().getString(R.string.five));
                                        }
                                    }
                                    chooseString.append(getResources().getString(R.string.or) + "||" + getResources().getString(R.string.back));//2
                                }
                                mIsListening = true;
                                ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                                isContainMultiple = true;
                                callTTS();
                            }

                        } else {
                            mIsListening = true;
                            ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
                            currentlyReadPhoneNumber = false;
                            callTTS();
                        }

                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else {
                        mIsListening = true;
                        ttsMessage = contactName + getResources().getString(R.string.not_address_book) + getResources().getString(R.string.name_or_number);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                } else if (isVoiceSmartReadyToSpeak == true && isYouWantToCall == true && !isNumeric(strResult.toString()) && !isCallToContactPerson && isContainMultiple) {
                    isContainMultiple = false;
                    if ((nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_one)) || strResult.equalsIgnoreCase("đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_contact)) || strResult.equalsIgnoreCase("Sự tiếp xúc đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.one)) || strResult.equalsIgnoreCase("Thứ nhất")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_1st)) || strResult.equalsIgnoreCase("Liên hệ lần thứ nhất")))
                            || (nameArrayList.size() > 0 && strResult.equalsIgnoreCase(nameArrayList.get(0)))) {
                        callMultiple(0, "call");
                    } else if ((nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_one)) || strResult.equalsIgnoreCase("cái thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_contact)) || strResult.equalsIgnoreCase("lần liên lạc thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.two)) || strResult.equalsIgnoreCase("Thứ 2")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_2nd)) || strResult.equalsIgnoreCase("Liên hệ lần 2")))
                            || (nameArrayList.size() > 1 && strResult.equalsIgnoreCase(nameArrayList.get(1)))) {
                        callMultiple(1, "call");
                    } else if ((nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_one)) || strResult.equalsIgnoreCase("thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_contact)) || strResult.equalsIgnoreCase("thứ ba liên lạc")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.three)) || strResult.equalsIgnoreCase("Thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_3rd)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 3")))
                            || (nameArrayList.size() > 2 && strResult.equalsIgnoreCase(nameArrayList.get(2)))) {
                        callMultiple(2, "call");
                    } else if ((nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_one)) || strResult.equalsIgnoreCase("ra một")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_contact)) || strResult.equalsIgnoreCase("thứ tư liên lạc")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_4th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ tư")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.four)) || strResult.equalsIgnoreCase("Thứ 4")))
                            || (nameArrayList.size() > 3 && strResult.equalsIgnoreCase(nameArrayList.get(3)))) {
                        callMultiple(3, "call");
                    } else if ((nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_one)) || strResult.equalsIgnoreCase("thứ năm")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_contact)) || strResult.equalsIgnoreCase("thứ năm liên lạc")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_5th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 5")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.five)) || strResult.equalsIgnoreCase("Thứ 5")))
                            || (nameArrayList.size() > 4 && strResult.equalsIgnoreCase(nameArrayList.get(4)))) {
                        callMultiple(4, "call");
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.back)) || strResult.equalsIgnoreCase((getResources().getString(R.string.back1)))) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.name_or_number);
                        initializeVariables(true, false, true, false, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                        isContainMultiple = true;
                        callTTS();
                    }
                }

                // Make a Call
                else if (isYouWantToCall == true && isCallToContactPerson == true) {
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.yes)) || strResult.equalsIgnoreCase(getResources().getString(R.string.yeah)) || strResult.equalsIgnoreCase(getResources().getString(R.string.confirm)) || strResult.equalsIgnoreCase(getResources().getString(R.string.agree))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.correct)) || strResult.equalsIgnoreCase(getResources().getString(R.string.ok))
                            || strResult.equalsIgnoreCase("Vâng") || strResult.equalsIgnoreCase("Vâng")
                            || strResult.equalsIgnoreCase("chính xác") || strResult.equalsIgnoreCase("được")) {

                        mIsListening = true;
                        currentlyReadPhoneNumber = false;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        manager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE); // start listening to the phone changes
                        callFromApp = true;
                        try {

                            isCallStarted = true;
                            makeCall();
                            isFinishing = true;
                        } catch (Exception e) {
                            Log.e("Make a Call", e.getMessage());
                        }
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.no)) || strResult.equalsIgnoreCase("Không")
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.back)) || strResult.equalsIgnoreCase("quay lại") || strResult.equalsIgnoreCase("trở lại")
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.wrong)) || strResult.equalsIgnoreCase("sai rồi")
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.sorry)) || strResult.equalsIgnoreCase("lấy làm tiếc")
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.not_correct)) || strResult.equalsIgnoreCase("không chính xác")) {
                        isCallToContactPerson = false;
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.name_or_number);
                        initializeVariables(true, false, true, false, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel_it)) || strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.quit)) || strResult.equalsIgnoreCase(getResources().getString(R.string.ignore))
                            || strResult.equalsIgnoreCase("hủy nó đi") || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("thoái lui") || strResult.equalsIgnoreCase("bỏ qua")) {
                        //								isNewSMSAction = true;
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.call_cancelled);
                        currentlyReadPhoneNumber = false;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        callTTS();
//                        reStartService();
                        isFinishing = true;
//                        Process.killProcess(Process.myPid());

//                        startActivity(new Intent(this,MainActivity.class));
                    }
                }

                // SMS Action Trigger
                else if ((isVoiceSmartReadyToSpeak == true && !isYouWantToText && (strResult.equalsIgnoreCase(getResources().getString(R.string.message)) || strResult.equalsIgnoreCase("tin nhắn") || strResult.equalsIgnoreCase(getResources().getString(R.string.new_message))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantToText && (strResult.equalsIgnoreCase(getResources().getString(R.string.write_message)) || strResult.equalsIgnoreCase("viết tin nhắn")))) {
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.name_or_number);
                    currentlyReadPhoneNumber = false;
                    initializeVariables(true, true, false, false, false, false, false, false, false, false, false);
                    callTTS();
                }

                // Read Phone Number Before send an SMS
                else if (isVoiceSmartReadyToSpeak == true && isYouWantToText == true && isNumeric(strResult.toString()) && !isSendTextToContactPerson) {
                    isSendTextToContactPerson = true;
                    mIsListening = true;
                    phoneNumber = strResult.toString();
                    removeSpecialCharFromPhoneNumber();
                    //							Log.d("Phone Number = ", ""+phoneNumber);
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.you_said) + phoneNumberWithSpace + getResources().getString(R.string.confirm_return);
                    currentlyReadPhoneNumber = true;
                    callTTS();
                }

                // Read Contact Name Before send an SMS
                else if (isVoiceSmartReadyToSpeak == true && isYouWantToText == true && !isNumeric(strResult.toString()) && !isSendTextToContactPerson && !isContainMultiple) {
                    contactName = strResult.toLowerCase();
                    getPhoneNumber(getApplicationContext(), contactName);
                    if (nameArrayList.size() > 0) {

                        if (contactIdArrayList.size() > 0) {

                            if (nameArrayList.size() == 1) {
                                //									Log.d("Phone = ", ""+phoneNumberArrayList.get(0));
                                ArrayList<String> phone = new ArrayList<String>();
                                phone = getCurrentPhoneNumber(contactIdArrayList.get(0));

                                if (phone.size() > 0) {
                                    phoneNumber = phone.get(0);
                                    removeSpecialCharFromPhoneNumber();
                                    name = contactName;

                                    isSendTextToContactPerson = true;
                                    mIsListening = true;
                                    ttsMessage = getResources().getString(R.string.you_said) + nameArrayList.get(0) + getResources().getString(R.string.confirm_return);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                } else {
                                    mIsListening = true;
                                    ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                }
                            } else {
                                sb = new StringBuffer();
                                chooseString = new StringBuffer();
                                if (nameArrayList.size() > 5) {
                                    for (int noA = 0; noA < 5; noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                    }
                                    chooseString.append(getResources().getString(R.string.one_five));
                                } else {
                                    for (int noA = 0; noA < nameArrayList.size(); noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                        if (noA == 0) {
                                            chooseString.append(getResources().getString(R.string.one));
                                        } else if (noA == 1) {
                                            chooseString.append("||" + getResources().getString(R.string.two));
                                        } else if (noA == 2) {
                                            chooseString.append("||" + getResources().getString(R.string.three));
                                        } else if (noA == 3) {
                                            chooseString.append("||" + getResources().getString(R.string.four));
                                        } else if (noA == 4) {
                                            chooseString.append("||" + getResources().getString(R.string.five));
                                        }
                                    }
                                    chooseString.append(getResources().getString(R.string.or) + "||" + getResources().getString(R.string.back));//3
                                }
                                mIsListening = true;
                                ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                                isContainMultiple = true;
                                callTTS();
                            }

                        } else {
                            mIsListening = true;
                            ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
                            currentlyReadPhoneNumber = false;
                            callTTS();
                        }
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else {
                        mIsListening = true;
                        ttsMessage = contactName + getResources().getString(R.string.not_address_book) + getResources().getString(R.string.name_or_number);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                } else if (isVoiceSmartReadyToSpeak == true && isYouWantToText == true && !isNumeric(strResult.toString()) && !isSendTextToContactPerson && isContainMultiple) {
                    isContainMultiple = false;
                    if ((nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_one)) || strResult.equalsIgnoreCase("đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_contact)) || strResult.equalsIgnoreCase("Sự tiếp xúc đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.one)) || strResult.equalsIgnoreCase("Thứ nhất")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_1st)) || strResult.equalsIgnoreCase("Liên hệ lần thứ nhất")))
                            || (nameArrayList.size() > 0 && strResult.equalsIgnoreCase(nameArrayList.get(0)))) {
                        callMultiple(0, "text");
                    } else if ((nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_one)) || strResult.equalsIgnoreCase("cái thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_contact)) || strResult.equalsIgnoreCase("lần liên lạc thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.two)) || strResult.equalsIgnoreCase("Thứ 2")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_2nd)) || strResult.equalsIgnoreCase("Liên hệ lần 2")))
                            || (nameArrayList.size() > 1 && strResult.equalsIgnoreCase(nameArrayList.get(1)))) {
                        callMultiple(1, "text");
                    } else if ((nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_one)) || strResult.equalsIgnoreCase("thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_contact)) || strResult.equalsIgnoreCase("thứ ba liên lạc")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.three)) || strResult.equalsIgnoreCase("Thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_3rd)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 3")))
                            || (nameArrayList.size() > 2 && strResult.equalsIgnoreCase(nameArrayList.get(2)))) {
                        callMultiple(2, "text");
                    } else if ((nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_one)) || strResult.equalsIgnoreCase("ra một")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_contact)) || strResult.equalsIgnoreCase("thứ tư liên lạc")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_4th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ tư")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.four)) || strResult.equalsIgnoreCase("Thứ 4")))
                            || (nameArrayList.size() > 3 && strResult.equalsIgnoreCase(nameArrayList.get(3)))) {
                        callMultiple(3, "text");
                    } else if ((nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_one)) || strResult.equalsIgnoreCase("thứ năm")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_contact)) || strResult.equalsIgnoreCase("thứ năm liên lạc")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_5th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 5")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.five)) || strResult.equalsIgnoreCase("Thứ 5")))
                            || (nameArrayList.size() > 4 && strResult.equalsIgnoreCase(nameArrayList.get(4)))) {
                        callMultiple(4, "text");
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.back)) || strResult.equalsIgnoreCase((getResources().getString(R.string.back1)))) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.name_or_number);
                        initializeVariables(true, false, true, false, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                        isContainMultiple = true;
                        callTTS();
                    }
                }

                // Check text send to contact name / number. If detect "Yes"... say "please dictate your text"
                else if ((isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.agree))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.ok))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.confirm))))) {
                    isYouWantToText = true;
                    isYouWantToPhoneNumberRead = true;
                    isYouWantToMessageRead = true;
                    isYouWantToReply = true;
                    isYouWantToSpeechText = false;
                    mIsListening = true;
                    ttsMessage = "" + getResources().getString(R.string.dictate_text);
                    currentlyReadPhoneNumber = false;
                    callTTS();
                }

                // Check text send to contact name / number. If detect "No"... say "please say a name in your address book or a phone number"
                else if ((isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.back))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.back1))))) {
                    isSendTextToContactPerson = false;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.name_or_number);
                    currentlyReadPhoneNumber = false;
                    initializeVariables(true, true, false, false, false, false, false, false, false, false, false);
                    callTTS();
                } else if ((isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase("hủy bỏ")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.close)) || strResult.equalsIgnoreCase("gần")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.exit)) || strResult.equalsIgnoreCase("lối thoát")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantToText && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.finish)) || strResult.equalsIgnoreCase("hoàn thành")))) {
                    //        isNewSMSAction = true;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.text_cancelled);
                    isSendTextToContactPerson = false;
                    initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                    callTTS();
//                    reStartService();
                    isFinishing = true;
                }

                //Check user want to read incoming message. //If detect "Yes", Read message content
                else if (isYouWantToPhoneNumberRead && !isYouWantToMessageRead && !isYouWantToReadMyText) {
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.read_the_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.read_the_news)) || strResult.equalsIgnoreCase(getResources().getString(R.string.read)) || strResult.equalsIgnoreCase(getResources().getString(R.string.yes1))) {
                        isYouWantToMessageRead = true;
                        mIsListening = true;
                        ttsMessage = message + getResources().getString(R.string.reply_repeat_ignore_callback);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.ignore)) || strResult.equalsIgnoreCase(getResources().getString(R.string.escape)) || strResult.equalsIgnoreCase(getResources().getString(R.string.stop))) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.good_bye);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else {
                        mIsListening = false;
                        startSpeechRecognition();
//								startVoiceRecognition();
                    }
                }

                //Check  user want to reply or repeat or call back.
                else if (isYouWantToPhoneNumberRead && isYouWantToMessageRead && !isYouWantToReply && !isYouWantToReadMyText) {
                    // If detected "reply" say " please dictate your text"
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.reply)) || strResult.equalsIgnoreCase(getResources().getString(R.string.write_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.write_message1)) || strResult.equalsIgnoreCase(getResources().getString(R.string.message))) {
                        isYouWantToText = true;
                        mIsListening = true;
                        isYouWantToReply = true;
                        ttsMessage = getResources().getString(R.string.dictate_text);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                    // If detected "repeat"  again read the message content
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.repeat)) || strResult.equalsIgnoreCase(getResources().getString(R.string.reread)) || strResult.equalsIgnoreCase(getResources().getString(R.string.reread_the_message))) {
                        mIsListening = true;
                        if (name != null) {
                            ttsMessage = getResources().getString(R.string.text_message_from) + name + message + getResources().getString(R.string.reply_repeat_ignore_callback);
                            currentlyReadPhoneNumber = false;
                        } else {
                            phoneNumberMakeReadable();
                            ttsMessage = getResources().getString(R.string.text_message_from) + phoneNumberWithSpace + message + getResources().getString(R.string.reply_repeat_ignore_callback);
                            currentlyReadPhoneNumber = true;
                        }
                        callTTS();
                    }
                    // If detected "call back", trigger call action
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.call_back)) || strResult.equalsIgnoreCase(getResources().getString(R.string.call)) || strResult.equalsIgnoreCase(getResources().getString(R.string.call1)) || strResult.equalsIgnoreCase(getResources().getString(R.string.phone_call))) {
                        manager.listen(myPhoneStateListener,
                                PhoneStateListener.LISTEN_CALL_STATE); // start listening to the phone changes
                        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                        callFromApp = true;

                        isCallStarted = true;
                        makeCall();
                        isFinishing = true;
//                        Process.killProcess(Process.myPid());
                    }
                    // If detected "cancel", Read "reply cancelled"
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel_it)) || strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.dont_send_it)) || strResult.equalsIgnoreCase(getResources().getString(R.string.ignore))
                            || strResult.equalsIgnoreCase("hủy nó đi") || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("không gửi nó") || strResult.equalsIgnoreCase("bỏ qua")) {
                        mIsListening = true;
                        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                        //								isNewSMSAction = true;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        ttsMessage = getResources().getString(R.string.text_cancelled);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;

                    }
                    // If nothing detected again would you like to reply, repeat, ignore or call back
                    else {
                        mIsListening = false;
                        startSpeechRecognition();
//								startVoiceRecognition();
                    }
                }

                // If user dictated the text, Read "you said "+ reply text + " say send it, re record it or cancel "
                else if (isYouWantToText && isYouWantToPhoneNumberRead && isYouWantToMessageRead && isYouWantToReply && !isYouWantToSpeechText) {
                    speechText = strResult;
                    isYouWantToSpeechText = true;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.you_said) + speechText + getResources().getString(R.string.rewrite_send_it);
                    currentlyReadPhoneNumber = false;
                    callTTS();
                }

                // Check user want to "send it, re record it or cancel
                else if (isYouWantToText && isYouWantToPhoneNumberRead && isYouWantToMessageRead && isYouWantToReply && isYouWantToSpeechText) {
                    //If detected "send it"
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.send_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.send)) || strResult.equalsIgnoreCase(getResources().getString(R.string.send1))
                            || strResult.equalsIgnoreCase("gửi đi") || strResult.equalsIgnoreCase("gửi tin nhắn")) {
                        if (PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
                            mIsListening = true;
                            sendSMS(phoneNumber, speechText);
                            nCount = nCount + 1;
                        } else {
                            showToastMessage("Invalid phone number");
                            //									isNewSMSAction = true;
                            initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                            mIsListening = true;
                            ttsMessage = "" + getResources().getString(R.string.invalid_text_cancelled);
                            currentlyReadPhoneNumber = false;
                            callTTS();
//                            reStartService();
                            isFinishing = true;
//                            Process.killProcess(Process.myPid());
                        }
                    }
                    //If detected "re record"
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.rewrite)) || strResult.equalsIgnoreCase(getResources().getString(R.string.rewrite_the_message)) || strResult.equalsIgnoreCase(getResources().getString(R.string.back1))) {
                        isYouWantToText = true;
                        isYouWantToReply = true;
                        isYouWantToSpeechText = false;
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.dictate_text);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                    //If detected "cancel it"
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.ignore)) || strResult.equalsIgnoreCase(getResources().getString(R.string.escape)) || strResult.equalsIgnoreCase(getResources().getString(R.string.stop))) {
                        mIsListening = true;
                        //								isNewSMSAction = true;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        ttsMessage = getResources().getString(R.string.text_cancelled);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
//                        Process.killProcess(Process.myPid());
//                        startActivity(new Intent(this, MainActivity.class));
                    }
                    //If nothing detected
                    else {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.you_said) + speechText + getResources().getString(R.string.rewrite_send_it);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                }

                //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Send Email Flow Started %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // Email Action Trigger
                else if ((isVoiceSmartReadyToSpeak == true && !isYouWantSendEmail && (strResult.equalsIgnoreCase(getResources().getString(R.string.send_email))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantSendEmail && (strResult.equalsIgnoreCase(getResources().getString(R.string.send_mail))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantSendEmail && (strResult.equalsIgnoreCase(getResources().getString(R.string.write_email))))
                        || (isVoiceSmartReadyToSpeak == true && !isYouWantSendEmail && (strResult.equalsIgnoreCase(getResources().getString(R.string.write_mail))))) {
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.name_or_email);
                    currentlyReadPhoneNumber = false;
                    initializeVariables(true, false, false, false, false, false, false, false, false, false, true);
                    callTTS();
                }

                // Read Email id Before send an SMS
                else if (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isEmailValid(strResult.toString()) && !isSendTextToContactPerson) {
                    isSendTextToContactPerson = true;
                    mIsListening = true;
                    phoneNumber = strResult.toString().replaceAll(" ", "");
                    ttsMessage = getResources().getString(R.string.you_said) + phoneNumber + getResources().getString(R.string.confirm_return);
                    currentlyReadPhoneNumber = true;
                    callTTS();
                }

                // Read Contact Name Before send an SMS
                else if (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && !isEmailValid(strResult.toString()) && !isSendTextToContactPerson && !isContainMultiple) {
                    contactName = strResult.toLowerCase();
                    getPhoneNumber(getApplicationContext(), contactName);
                    if (nameArrayList.size() > 0) {

                        if (contactIdArrayList.size() > 0) {

                            if (nameArrayList.size() == 1) {
                                //									Log.d("Phone = ", ""+phoneNumberArrayList.get(0));
                                ArrayList<String> phone = new ArrayList<String>();
                                phone = getCurrentEmail(contactIdArrayList.get(0));

                                if (phone.size() > 0) {
                                    phoneNumber = phone.get(0);
                                    removeSpecialCharFromPhoneNumber();
                                    name = contactName;

                                    isSendTextToContactPerson = true;
                                    mIsListening = true;
                                    ttsMessage = getResources().getString(R.string.you_said) + nameArrayList.get(0) + getResources().getString(R.string.confirm_return);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                } else {
                                    mIsListening = true;
                                    ttsMessage = contactName + getResources().getString(R.string.not_have_email_id) + getResources().getString(R.string.valid_contact_name);
                                    currentlyReadPhoneNumber = false;
                                    callTTS();
                                }
                            } else {
                                sb = new StringBuffer();
                                chooseString = new StringBuffer();
                                if (nameArrayList.size() > 5) {
                                    for (int noA = 0; noA < 5; noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                    }
                                    chooseString.append(getResources().getString(R.string.one_five));
                                } else {
                                    for (int noA = 0; noA < nameArrayList.size(); noA++) {
                                        sb.append(" | " + nameArrayList.get(noA));
                                        if (noA == 0) {
                                            chooseString.append(getResources().getString(R.string.one));
                                        } else if (noA == 1) {
                                            chooseString.append("||" + getResources().getString(R.string.two));
                                        } else if (noA == 2) {
                                            chooseString.append("||" + getResources().getString(R.string.three));
                                        } else if (noA == 3) {
                                            chooseString.append("||" + getResources().getString(R.string.four));
                                        } else if (noA == 4) {
                                            chooseString.append("||" + getResources().getString(R.string.five));
                                        }
                                    }
                                    chooseString.append(getResources().getString(R.string.or) + "||" + getResources().getString(R.string.back));//1
                                }
                                mIsListening = true;
                                ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                                isContainMultiple = true;
                                callTTS();
                            }

                        } else {
                            mIsListening = true;
                            ttsMessage = contactName + getResources().getString(R.string.not_have_phone_number) + getResources().getString(R.string.valid_contact_name);
                            currentlyReadPhoneNumber = false;
                            callTTS();
                        }
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else {
                        mIsListening = true;
                        ttsMessage = contactName + getResources().getString(R.string.not_address_book) + getResources().getString(R.string.name_or_number);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                } else if (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && !isNumeric(strResult.toString()) && !isSendTextToContactPerson && isContainMultiple) {
                    isContainMultiple = false;
                    if ((nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_one)) || strResult.equalsIgnoreCase("đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.first_contact)) || strResult.equalsIgnoreCase("Sự tiếp xúc đầu tiên")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.one)) || strResult.equalsIgnoreCase("Thứ nhất")))
                            || (nameArrayList.size() > 0 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_1st)) || strResult.equalsIgnoreCase("Liên hệ lần thứ nhất")))
                            || (nameArrayList.size() > 0 && strResult.equalsIgnoreCase(nameArrayList.get(0)))) {
                        callMultipleEmail(0, "text");
                    } else if ((nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_one)) || strResult.equalsIgnoreCase("cái thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.second_contact)) || strResult.equalsIgnoreCase("lần liên lạc thứ hai")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.two)) || strResult.equalsIgnoreCase("Thứ 2")))
                            || (nameArrayList.size() > 1 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_2nd)) || strResult.equalsIgnoreCase("Liên hệ lần 2")))
                            || (nameArrayList.size() > 1 && strResult.equalsIgnoreCase(nameArrayList.get(1)))) {
                        callMultipleEmail(1, "text");
                    } else if ((nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_one)) || strResult.equalsIgnoreCase("thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.third_contact)) || strResult.equalsIgnoreCase("thứ ba liên lạc")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.three)) || strResult.equalsIgnoreCase("Thứ ba")))
                            || (nameArrayList.size() > 2 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_3rd)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 3")))
                            || (nameArrayList.size() > 2 && strResult.equalsIgnoreCase(nameArrayList.get(2)))) {
                        callMultipleEmail(2, "text");
                    } else if ((nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_one)) || strResult.equalsIgnoreCase("ra một")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fourth_contact)) || strResult.equalsIgnoreCase("thứ tư liên lạc")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_4th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ tư")))
                            || (nameArrayList.size() > 3 && (strResult.equalsIgnoreCase(getResources().getString(R.string.four)) || strResult.equalsIgnoreCase("Thứ 4")))
                            || (nameArrayList.size() > 3 && strResult.equalsIgnoreCase(nameArrayList.get(3)))) {
                        callMultipleEmail(3, "text");
                    } else if ((nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_one)) || strResult.equalsIgnoreCase("thứ năm")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.fifth_contact)) || strResult.equalsIgnoreCase("thứ năm liên lạc")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.contact_5th)) || strResult.equalsIgnoreCase("Tiếp xúc lần thứ 5")))
                            || (nameArrayList.size() > 4 && (strResult.equalsIgnoreCase(getResources().getString(R.string.five)) || strResult.equalsIgnoreCase("Thứ 5")))
                            || (nameArrayList.size() > 4 && strResult.equalsIgnoreCase(nameArrayList.get(4)))) {
                        callMultipleEmail(4, "text");
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.stop)) || strResult.equalsIgnoreCase(getResources().getString(R.string.hang_up))
                            || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("dừng lại") || strResult.equalsIgnoreCase("treo lên")) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.hello_blue_genie);
                        Log.d("ttsMessage = ", "" + ttsMessage);

                        initializeVariables(false, false, false, true, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;
                    } else if (strResult.equalsIgnoreCase(getResources().getString(R.string.back)) || strResult.equalsIgnoreCase((getResources().getString(R.string.back1)))) {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.name_or_number);
                        initializeVariables(true, false, true, false, false, false, false, false, false, false, false);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    } else {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.you_have) + sb.toString() + getResources().getString(R.string.please_say) + chooseString;
                        isContainMultiple = true;
                        callTTS();
                    }
                }

                // Check Email send to contact name / number. If detect "Yes"... say "please dictate your text"
                else if ((isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.confirm))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.agree))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.ok))))) {
                    isYouWantSendEmail = true;
                    isYouWantToPhoneNumberRead = true;
                    isYouWantToMessageRead = true;
                    isYouWantToReply = true;
                    isYouWantToSpeechText = false;
                    mIsListening = true;
                    ttsMessage = "" + getResources().getString(R.string.say_your_text);
                    currentlyReadPhoneNumber = false;
                    callTTS();
                }

                // Check Email send to contact name / number. If detect "No"... say "please say a name in your address book or a email"
                else if ((isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.back))))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail == true && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.back1))))) {
                    isSendTextToContactPerson = false;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.name_or_email);
                    currentlyReadPhoneNumber = false;
                    initializeVariables(true, false, false, false, false, false, false, false, false, false, true);
                    callTTS();
                } else if ((isVoiceSmartReadyToSpeak == true && isYouWantSendEmail && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase("hủy bỏ")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.close)) || strResult.equalsIgnoreCase("gần")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.exit)) || strResult.equalsIgnoreCase("lối thoát")))
                        || (isVoiceSmartReadyToSpeak == true && isYouWantSendEmail && isSendTextToContactPerson == true && (strResult.equalsIgnoreCase(getResources().getString(R.string.finish)) || strResult.equalsIgnoreCase("hoàn thành")))) {
                    //        isNewSMSAction = true;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.email_cancelled);
                    isSendTextToContactPerson = false;
                    initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                    callTTS();
//                    reStartService();
                    isFinishing = true;
                }

                // If user dictated the email, Read "you said "+ reply text + " say send it, re record it or cancel "
                else if (isYouWantSendEmail && isYouWantToPhoneNumberRead && isYouWantToMessageRead && isYouWantToReply && !isYouWantToSpeechText) {
                    speechText = strResult;
                    isYouWantToSpeechText = true;
                    mIsListening = true;
                    ttsMessage = getResources().getString(R.string.you_said) + speechText + getResources().getString(R.string.rewrite_send_it_email);
                    currentlyReadPhoneNumber = false;
                    callTTS();
                }

                // Check user want to "send it, re record it or cancel
                else if (isYouWantSendEmail && isYouWantToPhoneNumberRead && isYouWantToMessageRead && isYouWantToReply && isYouWantToSpeechText) {
                    //If detected "send it"
                    if (strResult.equalsIgnoreCase(getResources().getString(R.string.send_email)) || strResult.equalsIgnoreCase(getResources().getString(R.string.send_mail)) || strResult.equalsIgnoreCase(getResources().getString(R.string.send)) || strResult.equalsIgnoreCase(getResources().getString(R.string.send1))) {

                        if (phoneNumber != null && phoneNumber.length() > 0) {
                            mIsListening = true;
                            new SendMail().execute("");
                            nCount = nCount + 1;
                            //Toast.makeText(getApplicationContext(), "Need to send Email", Toast.LENGTH_LONG);
                        } else {
                            showToastMessage("Invalid email id");
                            //									isNewSMSAction = true;
                            initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                            mIsListening = true;
                            ttsMessage = "" + getResources().getString(R.string.invalid_email_cancelled);
                            currentlyReadPhoneNumber = false;
                            callTTS();
                            //Toast.makeText(getApplicationContext(), "Need to send Email to " + phoneNumber, Toast.LENGTH_LONG);
//                            reStartService();
                            isFinishing = true;

//                            Process.killProcess(Process.myPid());
                        }
                    }
                    //If detected "re record"
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.rewrite)) || strResult.equalsIgnoreCase(getResources().getString(R.string.back)) || strResult.equalsIgnoreCase(getResources().getString(R.string.record))) {
                        isYouWantSendEmail = true;
                        isYouWantToReply = true;
                        isYouWantToSpeechText = false;
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.say_your_text);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                    //If detected "cancel it"
                    else if (strResult.equalsIgnoreCase(getResources().getString(R.string.cancel_it)) || strResult.equalsIgnoreCase(getResources().getString(R.string.cancel)) || strResult.equalsIgnoreCase(getResources().getString(R.string.exit))
                            || strResult.equalsIgnoreCase(getResources().getString(R.string.dont_send_it)) || strResult.equalsIgnoreCase(getResources().getString(R.string.ignore)) || strResult.equalsIgnoreCase(getResources().getString(R.string.reject))
                            || strResult.equalsIgnoreCase("hủy nó đi") || strResult.equalsIgnoreCase("hủy bỏ") || strResult.equalsIgnoreCase("lối thoát")
                            || strResult.equalsIgnoreCase("không gửi nó") || strResult.equalsIgnoreCase("bỏ qua") || strResult.equalsIgnoreCase("Từ chối")) {
                        mIsListening = true;
                        //								isNewSMSAction = true;
                        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
                        ttsMessage = getResources().getString(R.string.email_cancelled);
                        currentlyReadPhoneNumber = false;
                        callTTS();
//                        reStartService();
                        isFinishing = true;

//                        Process.killProcess(Process.myPid());
//                        startActivity(new Intent(this, MainActivity.class));
                    }
                    //If nothing detected
                    else {
                        mIsListening = true;
                        ttsMessage = getResources().getString(R.string.you_said) + speechText + getResources().getString(R.string.send_rerecord_cancel);
                        currentlyReadPhoneNumber = false;
                        callTTS();
                    }
                }
                //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Send Email Flow End %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


                else {
                    mIsListening = false;
                    startSpeechRecognition();
//							startVoiceRecognition();
                }
            }
        } else {
            mIsListening = false;
            startSpeechRecognition();
//			startVoiceRecognition();
        }


    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Send Email Flow Started %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.replaceAll(" ", "");
        ;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public ArrayList<String> getCurrentEmail(String contactId) {
        String mobile_email = "";
        String home_email = "";
        String work_email = "";
        String other_email = "";
        String custom_email = "";
        ContentResolver cr = getContentResolver();
        Cursor emails = cr.query(Email.CONTENT_URI, null, Phone.CONTACT_ID + " = " + contactId, null, null);

        if (phoneNumberArrayList.size() > 0) {
            phoneNumberArrayList.clear();
        }

        while (emails.moveToNext()) {
            String number = emails.getString(emails.getColumnIndex(Email.ADDRESS));
            int type = emails.getInt(emails.getColumnIndex(Email.TYPE));
            switch (type) {
                case Email.TYPE_MOBILE:
                    Log.e("TYPE_MOBILE ===>", "" + number);
                    mobile_email = number;
                    break;
                case Email.TYPE_HOME:
                    Log.e("TYPE_HOME ==> ", "" + number);
                    home_email = number;
                    break;
                case Email.TYPE_WORK:
                    Log.e("TYPE_WORK ====>", "" + number);
                    work_email = number;
                    break;
                case Email.TYPE_OTHER:
                    Log.e("TYPE_OTHER ====>", "" + number);
                    other_email = number;
                    break;
                case Email.TYPE_CUSTOM:
                    Log.e("TYPE_OTHER ====>", "" + number);
                    custom_email = number;
                    break;
            }
        }
        if (mobile_email != null && mobile_email.length() > 0) {
            phoneNumberArrayList.add(mobile_email);
        } else if (home_email != null && home_email.length() > 0) {
            phoneNumberArrayList.add(home_email);
        } else if (work_email != null && work_email.length() > 0) {
            phoneNumberArrayList.add(work_email);
        } else if (other_email != null && other_email.length() > 0) {
            phoneNumberArrayList.add(other_email);
        } else if (custom_email != null && custom_email.length() > 0) {
            phoneNumberArrayList.add(custom_email);
        }
        emails.close();
        return phoneNumberArrayList;
    }

    public void callMultipleEmail(int value, String type) {
        if (contactIdArrayList.size() > (value - 1)) {
            ArrayList<String> phone = new ArrayList<String>();
            phone = getCurrentEmail(contactIdArrayList.get(value));
            if (phone.size() > 0) {
                phoneNumber = phone.get(0);
                removeSpecialCharFromPhoneNumber();
                name = contactName;

                if (type.equalsIgnoreCase("call")) {
                    isCallToContactPerson = true;
                } else {
                    isSendTextToContactPerson = true;
                }

                mIsListening = true;
                ttsMessage = getResources().getString(R.string.you_said) + nameArrayList.get(value) + getResources().getString(R.string.correct_please_confirm);
            } else {
                mIsListening = true;
                ttsMessage = contactName + getResources().getString(R.string.not_have_email_id) + getResources().getString(R.string.valid_contact_name);
            }

        } else {
            mIsListening = true;
            ttsMessage = contactName + getResources().getString(R.string.not_have_email_id) + getResources().getString(R.string.valid_contact_name);
        }

        currentlyReadPhoneNumber = false;
        callTTS();
    }

    private class SendMail extends AsyncTask<String, Integer, Void> {

        private ProgressDialog progressDialog;
        private String result = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(TTSActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            finishSendEmail(result);
        }

        protected Void doInBackground(String... params) {
            Mail m = new Mail(Constants.username, Constants.password);

            String[] toArr = {phoneNumber};
            m.setTo(toArr);
            m.setFrom(Constants.username);
            m.setSubject(getResources().getString(R.string.email_subject));
            m.setBody(speechText);

            try {
                if (m.send()) {
                    Log.e("MailApp", getResources().getString(R.string.email_sent_successfully));
                    result = getResources().getString(R.string.email_sent_successfully);
                } else {
                    Log.e("MailApp", getResources().getString(R.string.email_was_not_sent));
                    result = getResources().getString(R.string.email_was_not_sent);
                }
            } catch (Exception e) {
                Log.e("MailApp", "Could not send email", e);
            }
            return null;
        }
    }

    public void finishSendEmail(String sendEmailResult) {
        //showToastMessage(sendEmailResult);
        mIsListening = true;
        ttsMessage = "" + sendEmailResult;
        currentlyReadPhoneNumber = false;
        initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
        callTTS();
//        reStartService();
        isFinishing = true;
    }
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Send Email Flow End %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    public void makeCall() {
        nCount = nCount + 1;
        int PERMISSION_ALL = 1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] PERMISSIONS = {Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE};
            if (hasPermissions(this, PERMISSIONS)) {

                session.createCrashSession(true);

                Log.d("Call Tag", "Test Call");
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(intent);

            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } else {
            Log.d("Call Tag", "Test Call");
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(intent);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && data != null) {
            initializeVariables(false, false, false, false, false, false, false, false, false, false, false);
            phoneNumber = getIntent().getExtras().getString("phoneNumber");
            message = getIntent().getExtras().getString("message");
            name = getContactName(context, phoneNumber);

            removeSpecialCharFromPhoneNumber();
            //				isNewSMSAction = false;

            // Start service from SMS Receiver
            mIsListening = true;
            if (voiceTriggerType.equalsIgnoreCase("0")) {
                isYouWantToPhoneNumberRead = true;
                if (name != null) {
                    ttsMessage = getResources().getString(R.string.text_from) + name + getResources().getString(R.string.like_read_it) + getResources().getString(R.string.yes_or_no);
                    currentlyReadPhoneNumber = false;
                } else {
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.text_from) + phoneNumberWithSpace + getResources().getString(R.string.like_read_it) + getResources().getString(R.string.yes_or_no);
                    currentlyReadPhoneNumber = true;
                }
            }
            // Start service from Incoming Call Receiver
            else if (voiceTriggerType.equalsIgnoreCase("1")) {
                if (name != null) {
                    ttsMessage = getResources().getString(R.string.call_from) + name + getResources().getString(R.string.repeat_connect_ignore);
                    currentlyReadPhoneNumber = false;
                } else {
                    phoneNumberMakeReadable();
                    ttsMessage = getResources().getString(R.string.call_from) + phoneNumberWithSpace + getResources().getString(R.string.repeat_connect_ignore);
                    currentlyReadPhoneNumber = true;
                }
            }
        }
        callTTS();
    }

    // Called from onCreate
    protected void createWakeLocks() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        fullWakeLock = powerManager.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "Loneworker - FULL WAKE LOCK");
        partialWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Loneworker - PARTIAL WAKE LOCK");
    }

    public void wakeDevice() {
        fullWakeLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }

}