package com.newvoicesmart.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.newvoicesmart.R;
import com.newvoicesmart.Utilities.Util;

public class InformationActivity extends BaseActivity {

    private Button mBTNNext;
    private Activity mActivity;
    private LinearLayout mLLNext;
    private TextView mTVEmail,mTVSendRecEmail,mTVCall,mTVSendRecCall,mTVSms,mTVSendRecSms,mTVSmartHome,mTVConnectAllDev,mTVAllByVoice,mTVMore,mtxt_email,txt_send_rec_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        //INITIALIZE VIEW
        initializeView();

        //FONT TYPE
        fontType();

        mLLNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.redirectFinishPage(mActivity,AppPurchaseActivity.class);
            }
        });
    }


    //INITIALIZE VIEW
    private void initializeView(){
        mActivity = InformationActivity.this;
        mBTNNext = findViewById(R.id.btn_next);
        mTVEmail = findViewById(R.id.txt_email);
        mTVSendRecEmail = findViewById(R.id.txt_send_rec_email);
        mTVCall = findViewById(R.id.txt_call);
        mTVSendRecCall = findViewById(R.id.txt_send_rec_call);
        mTVSms = findViewById(R.id.txt_sms);
        mTVSendRecSms = findViewById(R.id.txt_send_rec_sms);
        mTVSmartHome = findViewById(R.id.txt_smart_home);
        mTVConnectAllDev = findViewById(R.id.txt_connect_all_dev);
        mTVAllByVoice = findViewById(R.id.txt_all_by_voice);
        mTVMore = findViewById(R.id.txt_more);
        mLLNext = findViewById(R.id.linear_next);

    }
    //SET FONT FOR VIEW
    private void fontType(){
        Util.setFont(1,mActivity,mTVEmail,getString(R.string.email));
        Util.setFont(1,mActivity,mTVSendRecEmail,getString(R.string.send_rec_email));
        Util.setFont(1,mActivity,mTVCall,getString(R.string.call_txt));
        Util.setFont(1,mActivity,mTVSendRecCall,getString(R.string.send_rec_call));
        Util.setFont(1,mActivity,mTVSms,getString(R.string.sms));
        Util.setFont(1,mActivity,mTVSendRecSms,getString(R.string.send_rec_sms));
        Util.setFont(1,mActivity,mTVSmartHome,getString(R.string.smart_home));
        Util.setFont(1,mActivity,mTVConnectAllDev,getString(R.string.connect_all_device));
        Util.setFont(1,mActivity,mTVAllByVoice,getString(R.string.all_by_voice));
        Util.setFont(1,mActivity,mTVMore,getString(R.string.and_more));

    }


    public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            Util.redirectFinishPage(mActivity,SplashScreenActivity.class);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return  super.onKeyDown(keyCode,keyEvent);

    }
}
